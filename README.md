# YourMoments

### Overview
- - -

YourMoments is web application that allows people sharing moments from their lives.
Main idea is based on exchanging images and videos between users, but one particular Moment can be viewed by one user **only once**.                                                                                                       

App is available in polish language version.
Project finished 6th July 2017.

### Used technologies
- - -
##### Back-end
* Python
* Flask
* oauthlib

##### Front-end
* HTML
* CSS
* SASS
* JavaScript
* Vue.js
* Google Maps API

### Creating Moment
- - -
Process of creating new Moment is the most interesting part of whole service. It's extremely easy and brings a lot of fun!

####1. Choose video or image and upload it by drag'n'drop or file picker.
![Upload](https://media.giphy.com/media/6b7D9k02s5KxeIhTff/giphy.gif)

####2. Add some fancy filter to your Moment.
![Filter](https://media.giphy.com/media/edYHMOnzkVMf6IlHV7/giphy.gif)

####3. Say something deep and inspiring with use of brush...
![Customization](https://media.giphy.com/media/1lCFaqLQBWBi3QdnFF/giphy.gif)

####4. Only with purpose of erasing it now.
![Erase](https://media.giphy.com/media/7SQwbYQ2R4w7O7iCAs/giphy.gif)

####5. Add info about your current location and set users that will be able to see Moment.
![Info](https://media.giphy.com/media/2YnrFMdkXLQLVR1sjg/giphy.gif)

### How to set up a project
- - -
1. Obtain OAuth 2.0 credentials JSON file from Google API console and place it in `app/auth` directory with name `client_id.json`.

2. Obtain and set environmental variables that contain API keys of services that are necessary to proper work of app (Twitter, Google Maps, Google+ etc.).
3. Execute all commands presented below.


```
#!
    cd directory-with-project-files
    mkdir app/static/upload
    python manage.py db init
    python manage.py db migrate
    python manage.py db upgrade
```
