from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired

class LoginForm(FlaskForm):
    """
    Form class used to login user.

    Field/s:
        username - username of User to authenticate
        password - password of User to authenticate
    """
    renders = {
        "username": {
            "required": True,
            "pattern": "^[a-zA-Z0-9_.@]+$",
            "title": "Nazwa może składać się z liter, cyfr, kropek i podkreśleń",
            "minlength": 4,
            "maxlength": 64,
            "placeholder": "Nazwa użytkownika"
        },
        "password": {
            "required": True,
            "minlength": 6,
            "maxlength": 32,
            "pattern": "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).*$",
            "placeholder": "Hasło",
            "title": "Hasło musi zawierać małe i wielkie litery oraz cyfry."
        },
    }

    username = StringField("Nazwa użytkownika", validators=[InputRequired()])
    password = PasswordField("Hasło", validators=[InputRequired()])
