import httplib2
import requests
from . import auth
from .forms import LoginForm
from .models import Role, User
from .social_auth import (
    authenticate_user,
    credentials_to_json,
    get_twitter_oauth
)
from functools import wraps
from datetime import datetime
from flask import (
    redirect,
    url_for,
    render_template,
    current_app,
    request,
    jsonify,
    flash
)
from flask_login import current_user, login_user, login_required, logout_user
from sqlalchemy import or_
from app import db, login_manager
from oauth2client.client import flow_from_clientsecrets
from apiclient.discovery import build

BODY_CLASS = "auth-body"

@login_manager.user_loader
def load_user(u_id):
    return User.query.get(u_id)

def login_redirect(func):
    """
    Decorator that redirects to home view from auth views if user is already
    authenticated.

    Argument/s:
        func - view to decorate
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if current_user and current_user.is_authenticated:
            return redirect(url_for("main.home"))

        return func(*args, **kwargs)

    return wrapper

@auth.before_app_first_request
def before_first():
    Role.init_roles()

@auth.route("/")
@login_redirect
def index():
    """
    View that contains login form and basic info about the site.
    """
    form = LoginForm()
    return render_template("auth/index.html", form=form, body_class=BODY_CLASS)

@auth.route("/login", methods=["GET", "POST"])
@login_redirect
def login():
    """
    View that contains login form and handles standard user authentication.
    """
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter(or_(User.username == form.username.data,
                                    User.email == form.username.data)).first()

        if user and user.verify_password(form.password.data):
            login_user(user)
            return redirect(url_for("main.home"))

        flash("Dane logowania są niepoprawne, spróbuj ponownie.")

    if request.args.get("created_user"):
        flash('Możesz się zalogować.')

    return render_template("auth/login.html", form=form, body_class=BODY_CLASS)

@auth.route("/logout")
@login_required
def logout():
    """
    View that invokes function responsible for logging user out and redirects to
    index view.
    """
    logout_user()
    flash("Zostałeś pomyślnie wylogowany.")

    return redirect(url_for(".login"))

@auth.route("/register", methods=["GET"])
@login_redirect
def register():
    """
    View that contains register form and handles creating new user.
    """
    return render_template("auth/register.html",
                        body_class=BODY_CLASS,
                        SITE_KEY=current_app.config["RECAPTCHA_SITE_KEY"],
                        MAPS_KEY=current_app.config["GOOGLE_MAPS_API_KEY"])

@auth.route("/googleauth", methods=["GET"])
@login_redirect
def google_auth():
    """
    View that handles Google+ account authorization.
    """
    flow = flow_from_clientsecrets("app/auth/client_id.json",
        scope=["profile", "email", "https://www.googleapis.com/auth/plus.login"],
        redirect_uri=url_for('.google_auth', _external=True)
    )

    if not request.args.get("code"):
        return redirect(flow.step1_get_authorize_url())

    credentials = flow.step2_exchange(request.args.get("code"))
    http = credentials.authorize(httplib2.Http())

    service = build('plus', 'v1', http=http)

    user_data = service.people().get(userId="me").execute()

    try:
        login_user(authenticate_user("google", user_data))

    except:
        flash("Uzupełnij profil Google+ i spróbuj ponownie.")
        return redirect(url_for('auth.login'))

    return redirect(url_for('main.home'))

@auth.route("/twitterauth", methods=["GET"])
@login_redirect
def twitter_auth():
    """
    View that handles Twitter account authorization.
    """
    if not request.args.get('oauth_token'):
        r = requests.post("https://api.twitter.com/oauth/request_token",
                                                    auth=get_twitter_oauth())
        try:
            token = credentials_to_json(r.text)["oauth_token"]

        except:
            flash("Uzupełnij twitterowy profil i spróbuj ponownie.")
            return redirect(url_for('auth.login'))

        url = 'https://api.twitter.com/oauth/authenticate?oauth_token=' + token

        return redirect(url)

    data = {'oauth_verifier': request.args.get('oauth_verifier')}
    r = requests.post('https://api.twitter.com/oauth/access_token', data,
                                                    auth=get_twitter_oauth())

    cred = credentials_to_json(r.text)

    url = "https://api.twitter.com/1.1/account/verify_credentials.json"
    r = requests.get(url + "?include_email=true", auth=get_twitter_oauth(cred))

    try:
        login_user(authenticate_user("twitter", r.json()))

    except:
        flash("Uzupełnij twitterowy profil i spróbuj ponownie.")
        return redirect(url_for('auth.login'))

    return redirect(url_for("main.home"))

@auth.route("/check_user_data", methods=["POST"])
def check_user_data():
    """
    View responsible for checking if value of particular User attribute is
    available.
    """
    attr = request.get_json().get("attr_name")
    value = request.get_json().get("value")

    res = lambda x, y: (jsonify(x), y)

    try:
        if attr == "birthdate":
            value = datetime.strptime(value, "%Y-%m-%d").date()

    except ValueError:
        data = {
            "user": {
                "birthdate": "Birthdate must have format YYYY-MM-DD."
            }
        }
        return res(data, 403)

    try:
        data = {"available": User.check_availability(attr, value)}
        return res(data, 200)

    except:
        data = {"attr": "User model doesn't have attribute {}.".format(attr)}
        return res(data, 403)
