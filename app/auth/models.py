import getpass
import requests
from app import db
from flask import current_app, g
from flask_login import UserMixin
from flask_script import Command
from werkzeug.security import check_password_hash, generate_password_hash
from app.main.models import Moment

class Role(db.Model):
    """
    Model related with User, decides about particular User permissions.

    Field/s:
        id - primary key of Role
        name - name of Role
        default - boolean that decides if Role is default
        users - User objects related with particular Role object
    """
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, index=True)
    default = db.Column(db.Boolean, default=False, index=True)
    users = db.relationship("User", backref="role", lazy="dynamic")

    @staticmethod
    def init_roles():
        """
        Creates basic Role objects if any Role object hasn't been created.
        """
        if Role.query.count() == 0:
            roles = [
                {"name": "admin", "default": False},
                {"name": "user", "default": True},
            ]

            [db.session.add(Role(**r)) for r in roles]
            db.session.commit()

    def __repr__(self):
        return "<Role: name {}>".format(self.name)


class Invitation(db.Model):
    """
    Model that stores info about user who wants add to friends another user

    Field/s:
        id - primary key of Invitation
        sender - User object which invites another person
        invites - User object which is invited
        date - datetime when invitation has been send
    """
    __tablename__ = "invitations"
    id = db.Column(db.Integer, primary_key=True, index=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('users.id'), unique=False)
    sender = db.relationship("User", foreign_keys=sender_id,
                    backref=db.backref("posted_invitations", lazy="dynamic"))
    invites_id = db.Column(db.Integer, db.ForeignKey('users.id'), unique=False)
    invites = db.relationship("User", foreign_keys=invites_id,
                    backref=db.backref("received_invitations", lazy="dynamic"))
    date = db.Column(db.DateTime, default=db.func.current_timestamp())

    def __repr__(self):
        return "<Invitation: id {}>".format(self.id)


friendship = db.Table('friendship',
    db.Column('friend_1', db.Integer, db.ForeignKey("users.id")),
    db.Column('friend_2', db.Integer, db.ForeignKey("users.id"))
)

class User(UserMixin, db.Model):
    """
    Model that contains personal info about particular account.

    Field/s:
        id - primary key of User
        email - e-mail address of User
        username - username of User
        first_name - first name of User
        last_name - last name of User
        birthdate - birthday of User
        city_id - foreign key to City related with User
        role_id - foreign key to Role related with User
        password_hash - hashable version of password of User
        background - background image of User
        avatar - avatar image of User
        color - color in hex of User
        google_id - id of Google+ account related with User
        twitter_id - id of Twitter account related with User
    """
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    full_name = db.Column(db.String(64))
    birthdate = db.Column(db.Date)
    city_id = db.Column(db.Integer, db.ForeignKey("cities.id"))
    role_id = db.Column(db.Integer, db.ForeignKey("roles.id"))
    password_hash = db.Column(db.String(128))
    background = db.Column(db.String(256))
    avatar = db.Column(db.String(256))
    color = db.Column(db.String(8))
    google_id = db.Column(db.Text, unique=True, index=True)
    twitter_id = db.Column(db.Text, unique=True, index=True)
    friends = db.relationship(
        "User",
        secondary=friendship,
        primaryjoin=(id == friendship.c.friend_2),
        secondaryjoin=(id == friendship.c.friend_1),
        lazy="dynamic"
    )
    views = db.Column(db.Integer, default=0)

    @property
    def password(self):
        raise AttributeError("Password is write-only attribute.")

    @password.setter
    def password(self, password):
        """
        Sets User password by generating password hash based on password argument

        Argument/s:
            password - User password to generate
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Verifies if password is valid.

        Argument/s:
            password - User password to check
        """
        return check_password_hash(self.password_hash, password)

    @property
    def last_active(self):
        """
        Date of newest moment related with current user.
        """
        last_post = (g.user.friends_moments
                    .filter_by(author_id=self.id)
                    .order_by(Moment.date.desc()).limit(1).first())

        return last_post.date if last_post else None

    @staticmethod
    def check_availability(attr, value):
        """
        Checks if value of particular User field (mostly unique) is available.

        Argument/s:
            attr - name of User field to check
            value - value of field to check
        """
        if hasattr(User, attr):
            if isinstance(value, str):
                attr_ref = getattr(User, attr)
                return User.query.filter(attr_ref.ilike(value)).count() == 0

            else:
                return User.query.filter_by(**{attr: value}).count() == 0
        else:
            raise AttributeError(attr + " attribute of User isn't available.")

    def add_friend(self, friend):
        """
        Adds new friend

        Argument/s:
            friend - User object to add to friends
        """
        if friend == self:
            raise ValueError("User {} can't add itself to friends."
                                                    .format(self.username))

        if friend in self.friends.all():
            raise ValueError("User {} is already a friend of user {}."
                                .format(friend.username, self.username))

        self.friends.append(friend)
        friend.friends.append(self)

        db.session.add_all([self, friend])
        db.session.commit()

    def remove_friend(self, friend):
        """
        Removes friend

        Argument/s:
            friend - User object to remove from friends
        """
        if friend not in self.friends.all():
            raise ValueError("User {} is not a friend of user {}."
                                .format(friend.username, self.username))

        self.friends.remove(friend)
        friend.friends.remove(self)

        db.session.add_all([self, friend])
        db.session.commit()

    def generate_full_name(self):
        """
        Sets full name field value based on first and last name.
        """
        res = filter(lambda x: x and len(x), [self.first_name, self.last_name])
        self.full_name = " ".join(list(res))

    def __repr__(self):
        return "<User: username {}>".format(self.username)


class SuperUserCommand(Command):
    """
    Command subclass responsible for creating new User object with admin role.
    """
    def get_info(self):
        """
        Validates and returns info about admin user email and username.
        """
        while True:
            data = {
                "email": input("Podaj adres e-mail: "),
                "username": input("Podaj nazwę użytkownika: ")
            }

            if '@' not in data["email"] or data["email"].endswith('@'):
                print("Adres e-mail nie jest poprawny.")
                continue

            if not all([User.check_availability(k, data[k]) for k in data]):
                print("Adres e-mail i/lub nazwa jest już zajęta.")
                continue

            return data

    def get_password(self):
        """
        Validates and returns admin user password
        """
        while True:
            password = getpass.getpass("Podaj hasło: ")
            password_repeat = getpass.getpass("Powtórz hasło: ")

            if password != password_repeat:
                print("Hasła nie są jednakowe.")
                continue

            return password

    def run(self):
        """
        Gets basic info about user admin and creates it if data is valid.
        """
        Role.init_roles()

        data = self.get_info()
        password = self.get_password()
        data["role"] = Role.query.filter_by(name="admin").first()

        u = User(**data)
        u.password = password

        db.session.add(u)
        db.session.commit()

        print("Konto administratora zostało utworzone")


class City(db.Model):
    """
    Model that stores info about particular city.

    Field/s:
        id - primary key of City
        name - name of City
        region - region of City
        country - country of City
        latitude - latitude of City
        longitude - longitude of City
    """
    __tablename__ = "cities"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), index=True)
    region = db.Column(db.String(32), index=True)
    country = db.Column(db.String(32))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    place_id = db.Column(db.Text)
    users = db.relationship("User", backref="city", lazy="dynamic")

    @staticmethod
    def check_if_exists(place_id):
        """
        Checks if city with passed place id exists and returns tuple with
        boolean if city exists and dictionary with info about city.

        Argument/s:
            place_id - id of Google Place (city)
        """
        info = "place_id={}&key={}&language=pl".format(place_id,
                                current_app.config["GOOGLE_MAPS_API_KEY"])

        url = "https://maps.googleapis.com/maps/api/geocode/json?" + info
        res = requests.get(url).json()

        if res["status"] == "OK":
            try:
                localization = City.prepare_localization(res["results"])
            except:
                return False, {}

            return True, localization

        else:
            return False, {}

    @staticmethod
    def prepare_localization(results):
        """
        Gets dictionary with info about city from query results and extracts
        necessary information about particular city.

        Argument/s:
            results - list with results of Google Maps geocode query
        """
        # Get result which type of is locality (city)
        gen = (r for r in results if "locality" in r["types"] or
                            "administrative_area_level_3" in r["types"])
        l = next(gen)

        # Anonymous functions which helps to extract info from locality
        get_component = lambda x: next((c["long_name"] for c in l["address_components"] if c["types"][0] == x))
        get_coord = lambda x: l["geometry"]["location"][x]

        return {
            "name": get_component("locality"),
            "region": get_component("administrative_area_level_1"),
            "country": get_component("country"),
            "latitude": get_coord("lat"),
            "longitude": get_coord("lng")
        }

    def __repr__(self):
        return "<City: name {}, {}>".format(self.name, self.country)
