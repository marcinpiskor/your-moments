import re
import requests
from .serializers import (
    UserSerializer,
    UserFriendSerializer,
    InvitationPostedSerializer,
    InvitationReceivedSerializer,
    CityUserSerializer,
    CitySerializer
)
from app.api_v1.views import requires_auth
from flask import g, request, current_app
from flask_restful import Resource, reqparse
from sqlalchemy import or_
from app import api, db
from app.auth.models import User, City, Invitation, Role
from werkzeug.datastructures import FileStorage

class UserResource(Resource):
    """
    Resource responsible for displaying all users and creating new one.

    Instance attribute/s:
        parser - Request parser object with user fields arguments.
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('email', type=str, required=True)
        self.parser.add_argument('username', type=str, required=True)
        self.parser.add_argument('first_name', type=str, required=True)
        self.parser.add_argument('last_name', type=str, required=True)
        self.parser.add_argument('birthdate', type=str, required=True)
        self.parser.add_argument('recaptcha', type=str, required=False)
        self.parser.add_argument('city', type=str, required=True)
        self.parser.add_argument('password', type=str, required=True)
        self.parser.add_argument('background', type=FileStorage, required=False,
                                                            location='files')
        self.parser.add_argument('avatar', type=FileStorage, required=False,
                                                            location='files')
        self.parser.add_argument('color', type=str, required=False)

    def get(self):
        return [UserSerializer(u).data for u in User.query.all()]

    def post(self):
        data = self.parser.parse_args()

        try:
            self.validate_captcha(data.pop('recaptcha'))

        except ValueError as e:
            return {"recaptcha": e.args[0]}, 400

        return self.handle_user_create(data)

    def validate_captcha(self, recaptcha):
        """
        Checks if ReCAPTCHA key is valid.

        Argument/s:
            recaptcha - ReCAPTCHA key
        """
        if not current_app.config.get("TESTING"):
            url = "https://www.google.com/recaptcha/api/siteverify"
            data = {
                "secret": current_app.config["RECAPTCHA_SECRET_KEY"],
                "response": recaptcha
            }

            res = requests.post(url, data)

            if not res.json()["success"]:
                raise ValueError("ReCAPTCHA key isn't valid");

    def handle_user_create(self, data):
        """
        Creates new User and returns serialized version of that user or error
        that has been raised during creating user process.

        Argument/s:
            data - dictionary with info about new user account
        """
        user_serializer = UserSerializer(None)

        try:
            created_user = user_serializer.create_object(data)
            created_user.role = Role.query.filter_by(default=True).first()

        except Exception as e:
            return e.args[0], 400

        db.session.add(created_user)
        db.session.commit()

        return user_serializer.serialize_object(), 202


class UserItemResource(Resource):
    """
    Resource responsible for displaying particular user or updating it.
    """
    decorators = [requires_auth]

    def get(self, user_id):
        user_obj = User.query.get(user_id)

        if not user_obj:
            return {"status": "User object doesn't exist."}, 404

        return UserSerializer(user_obj).data

    def put(self, user_id):
        user_obj = User.query.get(user_id)

        if not user_obj:
            return {"status": "User object doesn't exist."}, 404

        if user_obj != g.user:
            return {"status": "You can modify only your account."}, 403

        return self.handle_user_update(user_obj)

    def handle_user_update(self, user_obj):
        """
        Updates user and returns serialized version of that updated user or e
        rror that has been raised during updating user process.

        Argument/s:
            user_obj - user object to update
        """
        user_serializer = UserSerializer(user_obj)

        try:
            user_serializer.update_object(self.prepare_request_data())
            return user_serializer.serialize_object(), 201

        except Exception as e:
            return e.args[0], 400

    def prepare_request_data(self):
        """
        Prepares dictionary with info to update user object.
        """
        data = {}
        [data.update(dict(r.items())) for r in [request.form, request.files]]

        return data


class CurrentUserResource(UserItemResource):
    """
    Resource responsible for displaying current user or updating it.
    """
    def get(self):
        return super().get(g.user.id)

    def put(self):
        return super().put(g.user.id)


class FriendsResource(Resource):
    """
    Resource responsible for displaying current user's friends or deleting it.

    Instance attribute/s:
        parser - Request parser object with id of friend to delete.
    """
    decorators = [requires_auth]

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('friend_id', type=int, required=True)

    def get(self):
        return [UserFriendSerializer(u).data for u in g.user.friends]

    def delete(self):
        friend_id = self.parser.parse_args()["friend_id"]
        friend = g.user.friends.filter_by(id=friend_id).first()

        if not friend:
            return {"status": "Friend with that id doesn't exist."}, 404

        g.user.remove_friend(friend)

        msg = "Friend {} has been removed.".format(friend.username)
        return {"status": msg}, 200


class InvitationResource(Resource):
    """
    Resource responsible for displaying current user's invitations and
    displaying new one.

    Instance attribute/s:
        parser - Request parser object with id of user to invite.
    """
    decorators = [requires_auth]

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=int, required=True)

    def get(self):
        received = self.serialize_invitations(InvitationReceivedSerializer,
                                                        "received_invitations")
        posted = self.serialize_invitations(InvitationPostedSerializer,
                                                        "posted_invitations")

        return {"received": received,"posted": posted}, 200

    def post(self):
        args = self.parser.parse_args()
        user = User.query.get(args["user_id"])

        if not user:
            return {"user_id": "User doesn't exist."}, 400

        if user == g.user:
            return {"user_id": "You cannot invite yourself."}, 400

        if user in g.user.friends:
            msg = "User {} is already your friend.".format(user.username)
            return {"user_id": msg}, 400

        if Invitation.query.filter_by(sender=g.user, invites=user).first():
            msg = "You've already invited user {}.".format(user.username)
            return {"user_id": msg}, 400

        if Invitation.query.filter_by(sender=user, invites=g.user).first():
            msg = "User {} has already invited you.".format(user.username)
            return {"user_id": msg}, 400

        inv = Invitation(sender=g.user, invites=user)

        db.session.add(inv)
        db.session.commit()

        return InvitationPostedSerializer(inv).data, 201

    def serialize_invitations(self, serializer, attr):
        """
        Prepares list with serialized invitations.

        Argument/s:
            serializer - invitation model serializer
            attr - name of user attribute query that is related with invitations
        """
        return [serializer(inv).data for inv in getattr(g.user, attr).all()]


class InvitationItemResource(Resource):
    """
    Resource responsible for displaying particular invitation or deleting it.
    """
    decorators = [requires_auth]

    def get(self, inv_id):
        inv = Invitation.query.get(inv_id)

        if not inv or (inv.sender != g.user and inv.invites != g.user):
            return {"status": "Invitation doesn't exist."}, 404

        if inv.sender == g.user:
            return InvitationPostedSerializer(inv).data, 200

        else:
            return InvitationReceivedSerializer(inv).data, 200

    def delete(self, inv_id):
        inv = Invitation.query.get(inv_id)

        if not inv or (inv.sender != g.user and inv.invites != g.user):
            return {"status": "Invitation doesn't exist."}, 404

        username = inv.invites.username
        db.session.delete(inv)
        db.session.commit()

        return {"status": "Invitation has been deleted."}, 200


class AcceptInvitationResource(Resource):
    """
    Resource responsible for accepting invitation.
    """
    decorators = [requires_auth]

    def get(self, inv_id):
        inv = Invitation.query.get(inv_id)

        if not inv or inv.invites != g.user:
            return {"status": "Invitation doesn't exist."}, 404

        inv.sender.add_friend(inv.invites)
        username = inv.sender.username

        db.session.delete(inv)
        db.session.commit()

        msg = "You've accepted user {} invitation.".format(username)
        return {"status": msg}, 200


class CockpitSearchResource(Resource):
    """
    Resource responsible for displaying all users and cities that match to query
    """
    decorators = [requires_auth]

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('query', required=True)

    def get(self):
        query = self.parser.parse_args()["query"] + "%"
        result = {"cities":[]}

        if re.match(r'^[0-9a-zA-Z._]+@[0-9a-zA-Z]+\.[a-zA-Z]{2,}$', query[:-1]):
            users = User.query.filter(User.email.ilike(query)).all()

        else:
            users = User.query.filter(or_(User.username.ilike(query),
                        User.full_name.ilike(query))).all()

            cities = City.query.filter(City.name.ilike(query)).all()
            result["cities"] = [CityUserSerializer(c).data for c in cities]

        result["users"] = [UserFriendSerializer(u).data for u in users]

        return result, 200


class CityItemResource(Resource):
    """
    Resource responsible for displaying particular city.
    """
    decorators = [requires_auth]

    def get(self, city_id):
        city = City.query.get(city_id)

        if not city:
            return {"city": {"id": "City with that id doesn't exist."}}, 404

        return CitySerializer(city).data


api.add_resource(UserResource, "/user")
api.add_resource(CurrentUserResource, "/currentuser")
api.add_resource(UserItemResource, "/user/<int:user_id>")
api.add_resource(CityItemResource, "/city/<int:city_id>")
api.add_resource(FriendsResource, "/friends")
api.add_resource(InvitationResource, "/invitation")
api.add_resource(InvitationItemResource, "/invitation/<int:inv_id>")
api.add_resource(AcceptInvitationResource, "/accept_invitation/<int:inv_id>")
api.add_resource(CockpitSearchResource, "/cockpit_search")
