import re
import string
import random
from .models import User, Role
from app import db
from datetime import datetime
from flask import current_app, request
from requests_oauthlib import OAuth1

class SocialAuth:
    """
    Class that contains basic methods related with authorizing social accounts,
    responsible for preparing info about currently authorized account.

    Class attribute/s:
        max_len - dictionary with lengths of important User fields

    Instance attribute/s:
        data - dictionary with info about authorized user, necessary to create
        new user.
    """
    max_len = {
        "email": User.email.type.length,
        "username": User.username.type.length
    }

    def __init__(self, data):
        self.data = self.prepare_data(data)

    def check_username(self, username_info):
        """
        Checks if social account nickname is set and if it's available. If not,
        special function responsible of generating alternative nickname would be
        invoked.

        Argument/s:
            username_info - dictionary with nickname and string that contains
            first and last name of currently authorized social account.
        """
        if username_info["basic"]:
            username = self.format_username(username_info["basic"])

            if User.check_availability("username", username):
                return username

        return self.generate_username(username_info["alter"])

    def generate_username(self, alt):
        """
        Generates username based on first and last name of currently authorized
        social account.

        Argument/s:
            alt - string that contains first and last name of currently authorized
            social account.
        """
        alt_username = self.format_username(alt)
        i = 0

        while True:
            if User.check_availability("username", alt_username):
                return alt_username

            else:
                i += 1

                if len(alt_username + str(i)) > self.max_len["username"]:
                    cut = self.max_len["username"] - len(alt_username + str(i))
                    alt_username = alt_username[:cut] + str(i)

                else:
                    alt_username += str(i)

    def format_username(self, username):
        """
        Formats username by adding to it random integer string if username is
        too short and replacing unallowed chars with dots.

        Argument/s:
            username - username to format
        """
        formatted = re.sub(r'[^a-zA-Z0-9_.]', '.', username)
        formatted = re.sub(r'[.][.]+', '.', formatted)[:self.max_len["username"]]

        if formatted.startswith('.'):
            formatted = formatted[1:]
        if formatted.endswith('.'):
            formatted = formatted[:-1]

        if len(formatted) < 6:
            get_digit = lambda: random.choice(string.digits)
            random_digits = [get_digit() for e in range(6 - len(formatted))]
            formatted += "".join(random_digits)

        return formatted

    def prepare_data(self, data):
        return data


class GoogleSocialAuth(SocialAuth):
    """
    Class that provides methods to authorize Google+ account.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def get_birthdate(user_data):
        """
        Gets birthdate of Google+ authorized account.

        Argument/s:
            user_data - dictionary with info about currently authorized Google+
            account
        """
        if user_data.get('birthday'):
            return datetime.strptime(user_data["birthday"], "%Y-%m-%d")

        elif user_data.get("ageRange"):
            if user_data.get("ageRange").get("min"):
                birth_year = user_data.get("ageRange")["min"]

            else:
                birth_year = user_data.get("ageRange")["max"]

            year = datetime.now().year
            return datetime(year - birth_year, 1, 1)

        else:
            return None

    @staticmethod
    def get_email(user_data):
        """
        Gets e-mail address of Google+ authorized account.

        Argument/s:
            user_data - dictionary with info about currently authorized Google+
            account
        """
        gen = (e for e in user_data.get('emails') if e["type"] == "account")
        email = next(gen)["value"]

        if len(email) > 0 and len(email) <= GoogleSocialAuth.max_len["email"]:
            return email

        raise ValueError("E-mail address isn't valid.")

    def prepare_data(self, user_data):
        """
        Prepares dictionary with all necessary info about Google+ authorized
        account to create new user.

        Argument/s:
            user_data - dictionary with info about currently authorized Google+
            account.
        """
        username_info = {
            "basic": user_data.get("nickname"),
            "alter": user_data.get("displayName")
        }

        return {
            "email": self.get_email(user_data),
            "username": self.check_username(username_info),
            "first_name": user_data["name"].get("givenName").title(),
            "last_name": user_data["name"].get("familyName").title(),
            "birthdate": self.get_birthdate(user_data).date(),
            "google_id": user_data["id"]
        }


class TwitterSocialAuth(SocialAuth):
    """
    Class that provides methods to authorize Twitter account.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def get_names(user_data):
        """
        Gets and returns tuple with first and last name of currently authorized
        Twitter account.

        Argument/s:
            user_data - dictionary with info about currently authorized Twitter
            account
        """
        names = user_data.get("name").title().split(" ")

        if len(names) > 1:
            return names[0], " ".join(names[1:])

        else:
            return "", ""

    @staticmethod
    def get_email(user_data):
        """
        Gets e-mail address of Google+ authorized account.

        Argument/s:
            user_data - dictionary with info about currently authorized Twitter
            account
        """
        email = user_data.get("email", "")

        if len(email) > 0 and len(email) <= TwitterSocialAuth.max_len["email"]:
            return email

        raise ValueError("Email address isn't valid.")

    def prepare_data(self, user_data):
        """
        Prepares dictionary with all necessary info about Twitter authorized
        account to create new user.

        Argument/s:
            user_data - dictionary with info about currently authorized Twitter
            account.
        """
        username_info = {
            "basic": user_data.get("screen_name"),
            "alter": user_data.get("name")
        }

        first_name, last_name = self.get_names(user_data)

        return {
            "email": self.get_email(user_data),
            "username": self.check_username(username_info),
            "first_name": first_name,
            "last_name": last_name,
            "twitter_id": user_data.get("id_str")
        }

auth_dict = {"twitter": TwitterSocialAuth, "google": GoogleSocialAuth}

def authenticate_by_id(name, user_data):
    """
    Checks if exists user with id of currently authorized social account and
    returns it.

    Argument/s:
        name - name of social app
        user_data - dictionary with info about currently authorized social
        account
    """
    social_id = {name + "_id": str(user_data["id"])}
    return User.query.filter_by(**social_id).first()

def authenticate_by_email(name, user_data):
    """
    Checks if exists user with e-mail of currently authorized social account and
    returns it.
    If user exists, updates id of particular social app of that user.

    Argument/s:
        name - name of social app
        user_data - dictionary with info about currently authorized social
        account
    """
    email = auth_dict[name].get_email(user_data)
    user = User.query.filter(User.email.ilike(email)).first()

    if user:
        setattr(user, name + "_id", str(user_data["id"]))
        db.session.add(user)
        db.session.commit()

    return user

def authenticate_user(name, user_data):
    """
    Based on currently authorized social account, returns user object that
    already exists or new one.

    Argument/s:
        name - name of social app
        user_data - dictionary with info about currently authorized social
        account
    """
    for func in [authenticate_by_id, authenticate_by_email]:
        user = func(name, user_data)

        if user:
            return user

    user = User(**auth_dict[name](user_data).data)
    user.role = Role.query.filter_by(default=True).first()
    user.generate_full_name()

    db.session.add(user)
    db.session.commit()

    return user

def credentials_to_json(data):
    """
    Converts Twitter OAuth credienials to JSON.

    Argument/s:
        data - string with credienials info, separeted by ampersand sign.
    """
    if "errors" in data:
        raise ValueError("Twitter auth error")

    res = {}

    for line in data.split('&'):
        key, value = line.split('=')
        res[key] = value

    return res

def get_twitter_oauth(cred={}):
    """
    Prepares OAuth object, necessary in Twitter account authorization process.

    Argument/s:
        cred - received Twitter credentials
    """
    consumer = current_app.config["TWITTER_CONSUMER_KEY"]
    secret = current_app.config["TWITTER_SECRET_KEY"]
    access = (cred.get('oauth_token') or request.args.get('oauth_token') or
                                    current_app.config["TWITTER_ACCESS_KEY"])
    token = (cred.get('oauth_token_secret') or
                                    current_app.config["TWITTER_TOKEN_KEY"])

    return OAuth1(consumer, secret, access, token)
