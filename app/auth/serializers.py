import re
import datetime
from .models import User, City, Role, Invitation
from app import db
from app.api_v1.serializers import ModelSerializer
from flask import current_app, g, current_app
from werkzeug.datastructures import FileStorage

class CityUserSerializer(ModelSerializer):
    """
    Nested serializer of City objects for User objects serializer.
    """
    model = City
    fields = ('id', 'name', 'region', 'country', 'latitude', 'longitude',
                                                                    'place_id')


class UserFriendSerializer(ModelSerializer):
    """
    Nested serializer of City objects for User objects serializer.
    """
    model = User
    fields = ('id', 'username', 'email', 'birthdate', 'city', 'color', 'views',
                'background', "avatar", 'first_name', 'last_name', "full_name")
    urls = ('avatar', 'background')
    city = CityUserSerializer


class UserMomentSerializer(ModelSerializer):
    """
    Nested serializer of User objects (friends) for User objects main
    serializer.
    """
    model = User
    fields = ('id', 'username', 'email', 'birthdate', 'city', 'color', 'views',
            'background', "avatar", 'first_name', 'last_name', 'last_active',
                                                                "full_name")
    city = CityUserSerializer
    urls = ('avatar', 'background')


class UserSerializer(ModelSerializer):
    """
    Main user serializer. Contains validate methods that allow create new User
    objects.
    """
    model = User
    fields = ('id', 'username', 'email', 'birthdate', 'city', 'color', 'views',
                'background', "avatar", 'first_name', 'last_name', "full_name",
                                                                    'friends')
    urls = ('avatar', 'background')

    city = CityUserSerializer
    friends = UserFriendSerializer

    def change_object_attributes(self, data):
        if data.get("password") and hasattr(g, "user"):
            self.change_password(data)

        super().change_object_attributes(data)

        if data.get("first_name") or data.get("last_name"):
            self.obj.generate_full_name()

    def validate_background(self, value):
        """
        Validates background file.
        File should have typical image extension (JPG, PNG or GIF).

        Argument/s:
            value - FileStorage object with background or empty value
        """
        return self.file_validation("background", value)

    def validate_avatar(self, value):
        """
        Validates avatar file.
        File should have typical image extension (JPG, PNG or GIF).
        """
        return self.file_validation("avatar", value)

    def file_validation(self, name, value):
        """
        Checks file validity and if it is proper, saves it.

        Argument/s:
            name - name of field
            value - FileStorage object
        """
        actual_value = getattr(self.obj, name)

        if actual_value and actual_value != value:
            self.files_to_delete.append(actual_value)

        if value:
            if not isinstance(value, FileStorage):
                raise ValueError({"user": {name: "Only file and empty value are allowed."}})

            if not self.check_extension(name, value):
                raise ValueError({"user": {name: "Invalid extension"}})

            data =  {
                "name": self.generate_unique_filename(value.filename),
                "file": value
            }

            self.files_to_save.append(data)

            return data["name"]

        return None

    def check_extension(self, name, value):
        """
        Checks file extension.

        Argument/s:
            name - name of field
            value - value of field (file)
        """
        allowed = current_app.config["ALLOWED_USER_EXTENTIONS"]
        return any([value.filename.endswith(ext) for ext in allowed])

    def validate_color(self, value):
        """
        Validates color in hexadecimal format.
        Color value should start with hash.

        Argument/s:
            value - string that contains user color
        """
        if value and not re.match(r'^[#][a-zA-Z0-9]{3,6}$', value):
            raise ValueError({"user": {"color": "Passed value is not hexadecimal format."}})

        return value

    def validate_username(self, value):
        """
        Validates username.
        Username should have proper length, chars and be unique.

        Argument/s:
            value - string that contains user username
        """
        if not (len(value) >= 4 and len(value) <= 64):
            raise ValueError({"user": {"username": "Username has to have minimum 4 and maximum 64 chars"}})

        if not re.match(r'^[a-zA-Z0-9_.]+$', value):
            raise ValueError({"user": {"username": "Username can contains only letters, digits, underscores and dotes."}})

        user = User.query.filter(User.username.ilike(value)).limit(1).first()

        if (user and hasattr(g, "user") and user != g.user) or user:
            raise ValueError({"user": {"username": "Username isn't available"}})

        return value

    def validate_email(self, value):
        """
        Validates e-mail address.
        email should have proper length, chars and be unique.

        Argument/s:
            value - string that contains user e-mail address
        """
        if not len(value) <= 64:
            raise ValueError({"user": {"email": "Email has to have maximum 64 chars"}})

        if not re.match(r'^[0-9a-zA-Z._]+@[0-9a-zA-Z]+\.[a-zA-Z]{2,}$', value):
            raise ValueError({"user": {"email": "Email isn't correct."}})

        user = User.query.filter(User.email.ilike(value)).limit(1).first()

        if (user and hasattr(g, "user") and user != g.user) or user:
            raise ValueError({"user": {"email": "Email isn't available."}})

        return value

    def validate_first_name(self, value):
        """
        Validates first name.
        First name should have proper length and chars.

        Argument/s:
            value - string that contains user first name
        """
        if not (len(value) >= 2 and len(value) <= 32):
            raise ValueError({"user": {"first_name": "First name has to have minimum 2 and maximum 32 chars"}})

        if not re.match(r'^[a-zA-Ząęśćżźłó]+$', value):
            raise ValueError({"user": {"first_name": "First name contains only letters."}})

        return value.title()

    def validate_last_name(self, value):
        """
        Validates last name.
        Last name should have proper length and chars.

        Argument/s:
            value - string that contains user last name
        """
        value = value.strip()

        if not (len(value) >= 2 and len(value) <= 32):
            raise ValueError({"user": {"last_name": "Last name has to have minimum 2 and maximum 32 chars"}})

        if not re.match(r'^[a-zA-Ząęśćżźłó][a-zA-Ząęśćżźłó ]+$', value):
            raise ValueError({"user": {"last_name": "Last name contains only letters and white spaces."}})

        return value.title()

    def validate_birthdate(self, value):
        """
        Validates birthdate.
        Birthdate should have proper format and real value.

        Argument/s:
            value - string that contains user e-mail birthdate (YYYY-MM-DD)
        """
        if not re.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2}', value):
            raise ValueError({"user": {"birthdate": 'Birthdate has to have format YYYY-MM-DD.'}})

        try:
            value = datetime.date(*[int(x) for x in value.split('-')])
            now = datetime.datetime.now().date()

            assert value < now and value.year > now.year - 125

        except:
            raise ValueError({"user": {"birthdate": 'Birthdate is invalid.'}})

        return value

    def validate_city(self, value):
        """
        Validates city.
        City should exist and not be blank.

        Argument/s:
            value - string that contains city place id
        """
        if not value:
            raise ValueError({"user": {"city": "Choose right localization."}})

        city = City.query.filter_by(place_id=value).limit(1).first()

        if city:
            return city

        exists, valid_city = City.check_if_exists(value)

        if not exists:
            raise ValueError({"user": {"city": "City doesn't exist."}})

        city = City(place_id=value, **valid_city)
        db.session.add(city)
        db.session.commit()

        return city

    def validate_password(self, password):
        """
        Validates password.
        Birthdate should have proper length and chars.

        Argument/s:
            password - string that contains user password
        """
        if not isinstance(password, str):
            raise ValueError({"user": {"password": "Password needs to be a string."}})

        if not (len(password) >= 6 and len(password) <= 32):
            raise ValueError({"user": {"password": "Password needs to have minimum 6 and maximum 32 chars."}})

        if not re.match(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)', password):
            raise ValueError({"user": {"password": "Password needs to have at least one small and big letter and one digit."}})

        return password

    def change_password(self, data):
        """
        Changes password of user to new one.

        Argument/s:
            data - dictionary with info about old and new password.
        """
        old_password = data.pop('old_password', None)

        if not old_password:
            raise ValueError({"user": {"old_password": "Old password is required."}})

        if g.user.password_hash and not g.user.verify_password(old_password):
            raise ValueError({"user": {"old_password": "Old password is invalid."}})

        self.obj.password = self.validate_password(data.pop("password", None))


class InvitationReceivedSerializer(ModelSerializer):
    """
    Invitation objects serializer related with invitations received by current
    user.
    """
    model = Invitation
    fields = ('id', 'sender')

    sender = UserFriendSerializer


class InvitationPostedSerializer(ModelSerializer):
    """
    Invitation objects serializer related with invitations posted by current
    user.
    """
    model = Invitation
    fields = ('id', 'invites')

    invites = UserFriendSerializer


class UserCitySerializer(ModelSerializer):
    """
    Nested serializer of City objects for User objects serializer.
    """
    model = User
    fields = ('id', 'username', 'email', 'birthdate', 'color', 'views',
                'background', "avatar", 'first_name', 'last_name', "full_name")
    urls = ('avatar', 'background')


class CitySerializer(ModelSerializer):
    """
    Main City objects serializer.
    """
    model = City
    fields = ('id', 'name', 'region', 'country', 'latitude', 'longitude',
                                                        'place_id', 'users')
    users = UserCitySerializer

    def serialize_object(self):
        serialized = super().serialize_object()

        moments = g.user.friends_moments.filter_by(city=self.obj).all()
        authors = set([m.author for m in moments])
        serialized["moments"] = [UserCitySerializer(a).data for a in authors]

        return serialized
