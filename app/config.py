import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get("SECRET_KEY") or "zaq1@WSX_M&29z"
    RECAPTCHA_SITE_KEY = os.environ.get("RECAPTCHA_SITE_KEY") or None
    RECAPTCHA_SECRET_KEY = os.environ.get("RECAPTCHA_SECRET_KEY") or None
    GOOGLE_MAPS_API_KEY = os.environ.get("GOOGLE_MAPS_API_KEY") or None
    TWITTER_CONSUMER_KEY = os.environ.get("TWITTER_CONSUMER_KEY") or None
    TWITTER_SECRET_KEY = os.environ.get("TWITTER_SECRET_KEY") or None
    TWITTER_ACCESS_KEY = os.environ.get("TWITTER_ACCESS_KEY") or None
    TWITTER_TOKEN_KEY = os.environ.get("TWITTER_TOKEN_KEY") or None
    SQLALCHEMY_COMMIT_ON_TEADOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class DevelopmentConfig(Config):
    DEBUG = True
    UPLOAD = os.path.join(BASE_DIR, "static/upload/")
    ALLOWED_USER_EXTENTIONS = set(['jpg', 'jpeg', 'png', 'gif'])
    ALLOWED_MOMENT_EXTENTIONS = set(['mp4', 'webm', '3gp', 'jpg', 'jpeg', 'png'
                                                                        'gif'])
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASE_DIR,
                                                            "data-dev.sqlite")


class LiveTestingConfig(Config):
    TESTING = True
    WTF_CSRF_ENABLED = False
    UPLOAD = os.path.join(BASE_DIR, "static/upload/")
    ALLOWED_USER_EXTENTIONS = set(['jpg', 'jpeg', 'png', 'gif'])
    ALLOWED_MOMENT_EXTENTIONS = set(['mp4', 'webm', '3gp', 'jpg', 'jpeg', 'png'
                                                                        'gif'])
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASE_DIR,
                                                            "data-test.sqlite")
    GOOGLE_USERNAME = os.environ.get("GOOGLE_USERNAME")
    GOOGLE_PASSWORD = os.environ.get("GOOGLE_PASSWORD")
    TWITTER_USERNAME = os.environ.get("TWITTER_USERNAME")
    TWITTER_PASSWORD = os.environ.get("TWITTER_PASSWORD")
    GECKO = os.environ.get("GECKO_DRIVER")


class TestingConfig(LiveTestingConfig):
    SERVER_NAME = "localhost"


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASE_DIR,
                                                            "data.sqlite")

config = {
    "dev": DevelopmentConfig,
    "test": TestingConfig,
    'live-test': LiveTestingConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig
}
