from .config import config
from flask import Flask, url_for, redirect
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

db = SQLAlchemy()
api = Api()
login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "auth.login"
login_manager.login_message = "Proszę się zalogować."

def create_app(config_name="default"):
    """
    Factory function that creates Flask app with configuration based on passed
    name.

    Argument/s:
        config_name - name of configuration to apply
    """
    app = Flask(__name__)
    app.config.from_object(config.get(config_name))

    db.init_app(app)
    login_manager.init_app(app)

    @app.errorhandler(404)
    def page_not_found(e):
        return redirect(url_for('auth.login'))

    from .auth import auth as auth_blueprint
    from .main import main as main_blueprint

    app.register_blueprint(auth_blueprint)
    app.register_blueprint(main_blueprint)

    api.prefix = '/api'

    from .api_v1 import views

    api.init_app(app)

    return app
