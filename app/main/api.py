from .models import Moment
from .serializers import MomentVideoSerializer, MomentImageSerializer
from app.api_v1.views import requires_auth
from flask import g, request
from flask_restful import Resource, reqparse
from app import api, db
from app.auth.models import User
from app.auth.serializers import UserFriendSerializer, UserMomentSerializer
from werkzeug.datastructures import FileStorage

class MomentResource(Resource):
    """
    Resource responsible for creating new Moment object.
    """
    decorators = [requires_auth]

    def post(self):
        data = self.prepare_request_data()

        for field in ["viewers", "content"]:
            if not data.get(field):
                return {'moment': {field: "Field is required."}}, 400

        if isinstance(data["content"], FileStorage) and data["content"].content_type.startswith('video'):
            # Duration of video will be based on file metadata, not user info.
            data.pop('duration', None)

            return self.create_moment(data, MomentVideoSerializer)

        else:
            if not data.get('duration'):
                return {'moment': {'duration': 'Duration is required for moments that contains images.'}}, 400

            return self.create_moment(data, MomentImageSerializer)

    def prepare_request_data(self):
        """
        Prepares dictionary with info to update user object.
        """
        data = {}
        [data.update(dict(r.items())) for r in [request.form, request.files]]

        return data

    def create_moment(self, data, serializer):
        """
        Creates new Moment and returns info about it or error that has been
        raised during creating moment process.

        Argument/s:
            data - dictionary with info about new user account
            serializer - serializer class responsible for creating object based
            on moment content filetype.
        """
        try:
            moment_serializer = serializer(None)
            created_moment = moment_serializer.create_object(data)
            created_moment.author = g.user

            db.session.add(created_moment)
            db.session.commit()

            return {'status': 'Moment has been created.'}, 202

        except Exception as e:
            return e.args[0], 400


class ActiveFriendsResource(Resource):
    """
    Resource responsible for displaying active friends (that friends which
    created Moment object with current user as viewer)

    Instance attribute/s:
        parser - Request parser object with info about pagination
    """
    decorators = [requires_auth]

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('page_number')
        self.parser.add_argument('per_page')

    def get(self):
        args = self.parser.parse_args()

        if all(list(args.values())):
            return self.paginate_get(args), 200

        else:
            return self.normal_get(), 200

    def normal_get(self):
        """
        Handles normal GET request without query parameters.
        Returns response with all serialized friends.
        """
        result = {"paginate": False}
        authors = set([m.author_id for m in g.user.friends_moments.all()])
        friends = g.user.friends.filter(User.id.in_(authors)).order_by(User.username).all()
        result["friends"] = [UserMomentSerializer(f).data for f in friends]

        return result

    def paginate_get(self, args):
        """
        Handles normal GET request with query parameters.
        Returns response with paginated user and the most popular user.

        Argument/s:
            args - dictionary with info about page number and amount of moments
            per page
        """
        info = int(args["page_number"]), int(args["per_page"])
        result = {
            "paginate": True,
            "has_next": False,
            "friends": [],
            "hottest": None
        }

        authors = set([m.author_id for m in g.user.friends_moments.all()])

        if len(authors) == 0:
            return result

        elif len(authors) == 1:
            hot_obj = g.user.friends.filter_by(id=authors.pop()).first()
            result["hottest"] = UserMomentSerializer(hot_obj).data
            return result

        else:
            query = g.user.friends.filter(User.id.in_(authors))

            hot_obj = query.order_by(User.views.desc()).limit(1).first()
            result["hottest"] = UserMomentSerializer(hot_obj).data

            pagin = (query.filter(User.id != hot_obj.id)
                                    .order_by(User.username).paginate(*info))
            result["friends"] = [UserMomentSerializer(p).data for p in pagin.items]
            result["has_next"] = pagin.has_next

            return result


class UserMomentsResource(Resource):
    """
    Resource responsible for displaying moments of particular users, one by one,
    sorted ascending by date.
    """
    decorators = [requires_auth]

    def get(self, user_id):
        friend = g.user.friends.filter_by(id=user_id).first()
        response = {
            "author": UserFriendSerializer(friend).data,
            "moment": None,
            "has_next": False
        }

        if friend:
            query = g.user.friends_moments.filter_by(author=friend)
            response["has_next"] = query.count() > 1
            moment = query.order_by(Moment.date).first()

            if moment:
                response["moment"] = MomentImageSerializer(moment).data

                friend.views += 1
                moment.viewers.remove(g.user)

                db.session.add_all([moment, friend])
                db.session.commit()

        return response

api.add_resource(MomentResource, "/moment")
api.add_resource(ActiveFriendsResource, "/activefriends")
api.add_resource(UserMomentsResource, "/user_moments/<int:user_id>")
