import os
import json
import base64
from datetime import datetime
from .models import Moment
from app import db
from app.auth.models import User, City
from flask import g, current_app
from hachoir.parser import createParser
from hachoir.metadata import extractMetadata
from werkzeug.datastructures import FileStorage
from app.api_v1.serializers import ModelSerializer
from app.auth.serializers import UserFriendSerializer, CityUserSerializer

MOMENT_EXTENSIONS = ["jpg", "jpeg", "png", "webm", "mp4", "3gp"]

class MomentBasicSerializer(ModelSerializer):
    """
    Parent Moment serializer that contains info that are mutual for image and
    video type moments.
    """
    model = Moment
    fields = ('id', 'content', 'duration', 'date', 'city')
    urls = ('content', )

    viewers = UserFriendSerializer
    city = CityUserSerializer

    def validate_viewers(self, value):
        """
        Validates viewers field.
        Viewers should be stringified list/array with id numbers of viewers.

        Argument/s:
            value - string that contains info about viewers
        """
        try:
            viewers = json.loads(value)
            assert isinstance(viewers, list)

        except:
            raise ValueError({'moment': {'viewers': 'Viewers should be a stringified list/array with id numbers of friends.'}})

        if len(viewers) == 0:
            raise ValueError({'moment': {'viewers': "Viewers can't be empty."}})

        return g.user.friends.filter(User.id.in_(set(viewers))).all()

    def validate_city(self, value):
        """
        Validates city.
        City should exist and not be blank.

        Argument/s:
            value - string that contains city place id
        """
        if not value:
            raise ValueError({"moment": {"city": "Choose right localization."}})

        city = City.query.filter_by(place_id=value).limit(1).first()

        if city:
            return city

        exists, valid_city = City.check_if_exists(value)

        if not exists:
            raise ValueError({"moment": {"city": "City doesn't exist."}})

        city = City(place_id=value, **valid_city)
        db.session.add(city)
        db.session.commit()

        return city

    def generate_moment_filename(self, file_ext):
        """
        Generates unique filename of moment based on time, author and extension

        Argument/s:
            file_ext - extension of Moment content file
        """
        now = datetime.now()

        today_date = "{d.year}_{d.month}_{d.day}".format(d=now)
        today_time = "{d.hour}_{d.minute}_{d.second}".format(d=now)

        return "{}_{}_{}.{}".format(g.user.username, today_date, today_time,
                                                                    file_ext)


class MomentVideoSerializer(MomentBasicSerializer):
    """
    Moment serializer that contains methods responsible for creating new Moment
    object with video content.
    """
    def change_object_attributes(self, data):
        try:
            for field in data:
                result = self.validate_field(field, data[field])
                setattr(self.obj, field, result)

        except Exception as e:
            self.handle_files()
            raise e

    def validate_content(self, value):
        """
        Validates moment video content.
        Video content should be video file with proper duration and extension.

        Argument/s:
            value - FileStorage object with video
        """
        video_ext = value.content_type.split('/')[-1]

        if video_ext.lower() in MOMENT_EXTENSIONS:
            filename = self.generate_moment_filename(video_ext)
            path = os.path.join(current_app.config["UPLOAD"], filename)
            value.save(path)
            self.files_to_delete.append(filename)

            self.obj.duration = self.get_video_duration(path)

            return filename

        else:
            raise ValueError({'moment': {'content': "Video has invalid extension (only MP4, WebM and 3gp allowed )"}})

    def get_video_duration(self, path):
        """
        Checks if video files is valid and has proper duration.

        Argument/s:
            path - string that contains info about new Moment content video path
        """
        metadata = extractMetadata(createParser(path))

        if not metadata:
            raise ValueError({'moment': {'content': "Video file is broken."}})

        duration = metadata.getValues('duration')[0].total_seconds()

        return self.validate_duration(duration)

    def validate_duration(self, value):
        """
        Validates Moment video duration.
        Moment video duration can't be bigger than 10 seconds.

        Argument/s:
            value - int with info about video duration
        """
        if value == 0 or value > 10:
            raise ValueError({'moment': {'duration': 'Duration of video moment has to be greater 0 and lesser than 11 seconds.'}})

        return value


class MomentImageSerializer(MomentBasicSerializer):
    """
    Moment serializer that contains methods responsible for creating new Moment
    object with image content.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.image_b64 = {}

    def handle_files(self, *args, **kwargs):
        if len(self.files_to_save) > 0:
            super().handle_files(*args, **kwargs)

        else:
            path = os.path.join(current_app.config["UPLOAD"],
                                                    self.image_b64["name"])
            value = base64.decodebytes(self.image_b64["value"].encode('ascii'))

            with open(path, 'wb') as moment_file:
                moment_file.write(value)

    def validate_duration(self, value):
        """
        Validates Moment image duration.
        Moment image duration can't be lesser than 1 second.

        Argument/s:
            value - string/int with info about video duration
        """
        value = int(value)

        if value < 1:
            raise ValueError({'moment': {'duration': 'Duration of image moment has to be equal or greater than 1.'}})

        return value

    def validate_content(self, value):
        """
        Validates moment image content.
        Image content should have proper extension or proper content.

        Argument/s:
            value - FileStorage object with image or string with image in
            base64 version.
        """
        if isinstance(value, FileStorage):
            extension = value.filename.split(".")[-1].lower()

            if extension in MOMENT_EXTENSIONS:
                file_info = {
                    "name": self.generate_moment_filename(extension),
                    "file": value
                }

                self.files_to_save.append(file_info)

                return file_info["name"]

            else:
                raise ValueError({'moment': {'content': "Image has invalid extension (only jpg, jpeg and PNG are allowed )"}})

        elif value:
            self.image_b64['value'] = self.prepare_moment_value(value)
            self.image_b64['name'] = self.generate_moment_filename('png')

            return self.image_b64['name']

        else:
            raise ValueError({'moment': {'content': "Content can't be empty."}})

    def prepare_moment_value(self, value):
        """
        Prepares value of image in base64 version.

        Argument/s:
            value - string with image in base64 version.
        """
        if value.startswith("data:image"):
            return value[value.find(','):]
        else:
            return value
