from app import db

moment_viewers = db.Table("moment_viewers",
    db.Column("user_id", db.Integer, db.ForeignKey("users.id")),
    db.Column("moment_id", db.Integer, db.ForeignKey("moments.id")),
    db.PrimaryKeyConstraint("user_id", "moment_id")
)

class Moment(db.Model):
    """
    Model that contains info about moment.

    Field/s:
        id - primary key of Moment
        content - filepath of Moment content (image or video)
        duration - duration of Moment
        date - date when Moment has been created
        author - author of Moment
        city - City object related with Moment
        viewers - list of Users that are allowed to see Moment
    """
    __tablename__ = "moments"
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(256))
    date = db.Column(db.DateTime, default=db.func.current_timestamp())
    duration = db.Column(db.Integer)
    author_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    author = db.relationship("User", foreign_keys=author_id,
                            backref=db.backref("moments", lazy="dynamic"))
    city_id = db.Column(db.Integer, db.ForeignKey('cities.id'))
    city = db.relationship("City", foreign_keys=city_id,
                            backref=db.backref('moments', lazy="dynamic"))
    viewers = db.relationship("User", secondary=moment_viewers, lazy="dynamic",
                        backref=db.backref("friends_moments", lazy="dynamic"))

    def __repr__(self):
        return "<Moment author: {}>".format(self.author.username)
