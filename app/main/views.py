from . import main
from flask import render_template, redirect, current_app
from flask_login import login_required

BODY_CLASS = "home-body"

@main.route("/home")
@login_required
def home():
    return render_template("main/home.html", body_class=BODY_CLASS,
                        MAPS_KEY=current_app.config["GOOGLE_MAPS_API_KEY"])
