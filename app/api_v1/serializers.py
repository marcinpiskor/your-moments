import os
import random
import string
import werkzeug
from datetime import date, datetime
from flask import url_for, current_app
from app import db
from app.config import BASE_DIR
from sqlalchemy.orm.dynamic import Query

class ModelSerializer:
    """
    Parent class for model serializers.
    Allows serialization to JSON or updates object

    Class attribute/s:
        urls - tuple with name of fields of model that stores file path

    Instances attribute/s:
        obj - object to serialize/update
        data - dictionary with serialized data
    """
    urls = ()

    def __init__(self, obj):
        """
        Argument/s:
            obj - obj to serialize/update
        """
        self.obj = obj
        self.data = self.serialize_object()
        self.files_to_save = []
        self.files_to_delete = []

    def serialize_object(self):
        """
        Creates dict with serialized data.
        """
        if self.obj:
            return {field: self.get_field(field) for field in self.fields}

        else:
            return {}

    def get_field(self, attr):
        """
        Serializes value of field.

        Argument/s:
            attr - name of field
        """
        value = getattr(self.obj, attr)
        nested_serializer = getattr(self, attr, None)

        if nested_serializer and value:
            return self.handle_nested_field(nested_serializer, value)

        elif isinstance(value, date):
            return self.handle_date_field(value)

        elif attr in self.urls and value:
            filename = os.path.join("upload/", value)
            return url_for("static", filename=filename, _external=True)

        else:
            return value

    def handle_nested_field(self, serializer, value):
        """
        Handles serializing of field with own serializer attached.

        Argument/s:
            serializer - nested serializer of field
            value - value of field
        """
        # Checking if field is query, which indicates that field contains many
        # elements
        if isinstance(value, Query):
            nested_data = []

            for obj in value.all():
                nested_data.append(serializer(obj).data)

            return nested_data

        else:
            return serializer(value).data

    def handle_date_field(self, value):
        """
        Handles serializing of date field.

        Argument/s:
            value - value of field (date or datetime object)
        """
        if isinstance(value, datetime):
            return value.isoformat() + 'Z'

        return str(value)

    def create_object(self, data):
        """
        Creates new object based on passed data.

        Argument/s:
            data - dictionary with data to modify
        """
        self.obj = self.model()
        self.change_object_attributes(data)

        return self.obj

    def update_object(self, data):
        """
        Updates object based on passed data.

        Argument/s:
            data - dictionary with data to modify
        """
        self.change_object_attributes(data)

        db.session.add(self.obj)
        db.session.commit()

        return self.obj

    def change_object_attributes(self, data):
        """
        Changes valies of attributes of object based on passed data

        Argument/s:
            data - dict with name of field as key and new values as value
        """
        for field in data:
            result = self.validate_field(field, data[field])
            setattr(self.obj, field, result)

        self.handle_files()

    def handle_files(self):
        """
        Handles file uploading and deleting.
        """
        for file_obj in self.files_to_save:
            path = os.path.join(current_app.config["UPLOAD"], file_obj["name"])
            file_obj["file"].save(path)

        for filename in self.files_to_delete:
            path = os.path.join(current_app.config["UPLOAD"], filename)

            if os.path.isfile(path):
                os.remove(path)

        self.files_to_save = []
        self.files_to_delete = []

    def generate_unique_filename(self, filename):
        """
        Generates unique filename to prevent files overriding.

        Argument/s:
            filename - name of file
        """
        secure_name = werkzeug.secure_filename(filename)
        name = secure_name.split(".")[0]
        ext = secure_name.split(".")[-1]
        chars = string.ascii_letters + string.digits

        check = lambda x: os.path.isfile(x)
        prop_name = lambda: "{}.{}".format(name, ext)

        while check(os.path.join(current_app.config["UPLOAD"], prop_name())):
            name += "".join([random.choice(chars) for x in range(4)])

        return prop_name()

    def validate_field(self, field, value):
        """
        Validates field value by invoking method that check validity of
        particular field.

        Argument/s:
            field - name of field
            value - value of field
        """
        validate_method = getattr(self, "validate_" + field, None)

        if validate_method:
            return validate_method(value)

        else:
            model_name = self.model.__tablename__[2:-3]
            error = {model_name: {field: "This field isn't allowed to change"}}

            raise AttributeError(error)
