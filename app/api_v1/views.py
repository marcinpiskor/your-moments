from functools import wraps
from flask import g, request
from flask_login import current_user
from app.auth.models import User, City, Invitation

def requires_auth(func):
    """
    Decorator function responsible for authorizing user to allow working on API.

    Argument/s:
        func - function to wrap
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:
            g.user = current_user
            return func(*args, **kwargs)

        auth = request.authorization

        if auth:
            user = User.query.filter_by(username=auth.username).first()

            if user and user.verify_password(auth.password):
                g.user = user
                return func(*args, **kwargs)

        return {"status": "Invalid credentials."}, 401

    return wrapper

from app.auth.api import api as auth_api
from app.main.api import api as main_api
