var path = require("path");
var webpack = require("webpack");

module.exports = {
  context: path.join(__dirname, 'src'),
  entry: {
    main: './main/main.js',
    auth: './auth/main.js'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle-[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: /src/,
      },
      {
    		test: /\.vue$/,
    		loader: 'vue-loader'
  		},
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: false
    }),
  ]
}
