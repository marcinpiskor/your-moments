import Vue from 'vue';
import App from "./App.vue";

window.onload = () => {
  // Getting random background image and loading it
  let back_url = `/static/images/${Math.floor(Math.random() * 11 + 1)}.jpg`;
  let im = new Image();

  im.onload = () => {
    let back = document.querySelector(".back");

    // Setting min height prevents from stretching background image on mobile
    // (e.g. during typing text or hiding URL bar on browser)
    back.style.minHeight = 1.3 * window.innerHeight + "px";
    back.style.backgroundImage = `url(${back_url})`;
    back.className += " visible";
  };

  im.src = back_url;

  if (document.title.startsWith("Zarejestruj się")) {
    new Vue({el: '#app',
      render: h => h(App)
    });
  }
}
