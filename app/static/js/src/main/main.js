import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from "./routes";
import App from "./App.vue";
import store from "./store";

window.onload = () => {
  Vue.use(VueRouter);

  let router = new VueRouter({
    routes: routes,
  });

  router.beforeEach((to, from, next) => {
    document.title = to.name + " - YourMoments";
    sessionStorage.setItem("last_page", from.path);
    next();
  });

  new Vue({el: '#app',
    router: router,
    store: store,
    render: h => h(App)
  });
}
