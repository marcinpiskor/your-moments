let date_props = [
  {name: "Minutes", single: "minutę", plural: "minut", special: "minuty", div: 60},
  {name: "Hours", single: "godzinę", plural: "godzin", special: "godziny", div: 60},
  {name: "Date", single: "dzień", plural: "dni", special: null, div: 24},
  {name: "Week", single: "tydzień", plural: "tygodni", special: "tygodnie", div: 7},
  {name: "FullYear", single: "rok", plural: "lat", special: "lata", div: 365},
]

export function pub_date() {
  /**
  * Converts date to human readable format by counting how many time has passed
  * from that day.
  * @return { String } date in human readable format.
  */
  let current_property;
  let actual_date = new Date();
  let hot_date = new Date(this[this.$options.name].last_active);

  let result = (actual_date - hot_date) / 1000;

  for (let p of date_props) {
    if ((result/p.div) > 1) {
      current_property = p;
      result /= p.div;
    }
    else {
      break;
    }
  };

  if (!!current_property) {
      result = Math.floor(result);
      let time = get_proper_form(current_property, result);

      return `${result} ${time} temu`;
  }

  return "Przed chwilą";
}

export function get_proper_form(property, result) {
  /**
  * Gets proper form of noun related with time.
  * @param { Object } property - object with info about property related with
  * time (day, week etc.)
  * @param { Number } result - amount of e.g. days, weeks.
  * @return { String } - proper form of noun.
  */
  if (result === 1) {
    return property.single;
  }

  else if (!!property.special) {
    let res = String(result);
    let pre_last = res.charAt(res.length-2);
    let last = parseInt(res.charAt(res.length-1));

    if (last >= 2 && last <= 4 && pre_last !== '1') return property.special;
  }

  return property.plural;
}

export function get_url(link, params) {
  /**
  * Creates URL with query parameters.
  * @param { String } link - main URL part.
  * @param { Object } params - query parameters.
  * @return { String } - URL with query parameters.
  */
  let url = window.location.href.match(/http:\/\/[a-zA-Z0-9.:]+/) + link;

  let query = Object.keys(params)
             .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(params[key]))
             .join("&")
             .replace(/%20/g, "+");

  return url + "?" + query;
}

export function moment_title() {
  return "Zobacz momenty użytkownika @" + this[this.$options.name].username;
}

export function profile_title() {
  return "Zobacz profil użytkownika @" + this[this.$options.name].username;
}

export function moment_link() {
  return {name: "Zobacz momenty", params: {id: this[this.$options.name].id}};
}

export function profile_link() {
  return {name: "Profil użytkownika", params: {id: this[this.$options.name].id}};
}
