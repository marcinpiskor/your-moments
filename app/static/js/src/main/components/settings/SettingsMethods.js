export function update_el(file) {
  let img = new Image();

  img.onload = () => {
    this.edit_data[this.$refs.file.name] = file;
    this.img_preview[this.$refs.file.name] = img.src;
    this.leave()
  }

  img.onerror = () => {
    this.$refs.file.value = null;
  }

  img.src = URL.createObjectURL(file);
}

export function check_availability(event) {
  let field = event.target;
  
  if (field.checkValidity()) {
    if (!this.user || field.value !== this.user[field.name]) {
      send_check_request.bind(this)(field);
    }
    else {
      this.availability[field.name] = true;
    }
  }
  else {
    this.availability[field.name] = false;
  }
}

function send_check_request(field) {
  let request = new Request(
    "/check_user_data",
    {
      method: "POST",
      body: JSON.stringify({attr_name: field.name, value: field.value}),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    }
  );

  fetch(request)
    .then(response => response.json())
    .then(data => {
      this.availability[field.name] = data.available;
    })
}

export function extract_error(error) {
  let extracted = error[Object.keys(error)[0]];

  return typeof(extracted) === "string" ? extracted : extract_error(extracted);
}
