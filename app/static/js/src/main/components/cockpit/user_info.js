import { get_proper_form } from "../moments/MomentMethods";

export function avatar_style() {
  return !!this.user.avatar ? {backgroundImage: `url(${this.user.avatar})`} : {};
}

export function first_info() {
  if (!!this.user.full_name && this.user.full_name.split(" ").length > 0) {
    return this.user.full_name;
  }
  else {
    return this.user.username;
  }
}

export function second_info() {
  if (!!this.user.city) {
    return `${this.user.city.name}, ${this.user.city.country}`;
  }
  else {
    let prop = {single: "wyświetlenie", plural: "wyświetleń", special: "wyświetlenia"}
    return this.user.views + " " + get_proper_form(prop, this.user.views);
  }
}

export function user_link() {
  return {name:"Profil użytkownika", params: {id: this.user.id}};
}

export function city_link() {
  return {name:"Profil miejsca", params: {id: this.user.city.id}};
}

export function get_random_color() {
  /**
  * Generates random color in RGB format for cities that doesn't have any
  * photos
  * @return { String } - RGB color prepared to style apply.
  */
  let num = () => Math.floor(Math.random() * 255);
  return `${num()}, ${num()}, ${num()}`;
}
