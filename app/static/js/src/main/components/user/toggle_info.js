export function toggle_aside() {
  let client_width = Math.max(document.documentElement.clientWidth, window.innerWidth);

  if (client_width > 700 && this.$refs.content.getBoundingClientRect().top < 10) {
    this.sticky_profile_info = true;
  }
  else {
    this.sticky_profile_info = false;
  }
}
