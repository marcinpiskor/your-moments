export default {
  check_extension(img_file) {
    /**
    * Checks if file has proper (image) extension.
    * @param { Object } img_file - File object.
    */
    let name = img_file.name.toLowerCase()
    let result = this.extensions.find(e => name.endsWith(e));

    return !!result;
  },
  file_change() {
    /**
    * Invokes function responsible for updating user or leaving D&D action.
    */
    let file = this.$refs.file.files[0];
    this.check_extension(file) ? this.update_el(file) : this.leave();
  },
  click() {
    this.$refs.file.click()
  },
  drag(event) {
    /**
    * Handles drag event action by setting proper text.
    * @param { Object } event - JS drag event object.
    */
    event.preventDefault();
    this.info = `Upuść ${this.el_name}.`;
  },
  leave() {
    /**
    * Handles drag leave event action by setting proper text.
    */
    this.info = `Wybierz ${this.el_name}.`;
  },
  drop(event) {
    /**
    * Handles drop event action by invoking function responsible for updating
    * user or leaving D&D action.
    * @param { Object } event - JS drop event object.
    */
    event.preventDefault();

    if (event.dataTransfer.files.length > 0) {
      let file = event.dataTransfer.files[0];
      this.check_extension(file) ? this.update_el(file) : this.leave();
    }
    else {
      this.leave();
    }
  },
  update_el(img_file) {
    /**
    * Changes text, prepares user data and invokes function responsible for
    * updating user data.
    * @param { Object } img_file - File object.
    */
    this.info = "Wysyłanie...";
    let form = new FormData();
    form.append(this.$refs.file.name, img_file);

    this.$store.dispatch("update_user", form);
  }
}
