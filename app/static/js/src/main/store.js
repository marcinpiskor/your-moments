import Vue from "vue";
import Vuex from "vuex";
import { extract_error } from "./components/settings/SettingsMethods";

Vue.use(Vuex);

const remove_invitation_from_store = (state, invitation, el, type) => {
  let remove_invitation = () => {
    const index = state.invitations[type].indexOf(invitation);
    state.invitations[type].splice(index, 1);
  }

  if (!!el) {
    el.className += " hidden";
    setTimeout(remove_invitation, 800);
  }
  else {
    remove_invitation();
  }
}

const fetch_info = (context) => {
  return () => {
    context.commit("fetch_user");
    context.commit("fetch_invitations");
  }
}

const handle_response = (response) => {
  if (!response.ok) throw new Error(response.statusText);
  return response.json();
};

const store = new Vuex.Store({
  state: {
    user: {
      avatar: null,
      background: null,
      birthdate: null,
      city: null,
      color: null,
      email: null,
      first_name: null,
      friends: [],
      id: 0,
      last_name: null,
      username: null,
      views: 0
    },
    invitations: {
      posted: [],
      received: []
    }
  },
  getters: {
    user(state) {
      return state.user;
    },
    friends(state) {
      return state.user.friends;
    },
    invitations(state) {
      return state.invitations;
    },
    invitations_length(state) {
      return state.invitations.length;
    },
    invitations_posted(state) {
      return state.invitations.posted;
    },
    invitations_received(state) {
      return state.invitations.received;
    }
  },
  mutations: {
    fetch_user(state, redirect=null) {
      let request = new Request(
        "/api/currentuser",
        {
          method: "GET",
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => response.json())
        .then(data => {
          state.user = data;
          if (!localStorage.getItem(data.username) && !!redirect) redirect();
        });
    },
    update_user(state, form) {
      let request = new Request(
        "/api/currentuser",
        {
          method: "PUT",
          body: form,
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => response.json())
        .then(data => {
          if (Object.keys(data).length > 1) {
            state.user = data;
          }
          else {
            let error_text = extract_error(data);

            switch(error_text) {
              case "Invalid extension":
                alert("Plik tła/awataru ma złe rozszerzenie lub jest uszkodzony.")
                break;

              case "Passed value is not hexadecimal format.":
                alert("Kolor musi być w formacie heksadecymalnym.")
                break

              case "Username has to have minimum 4 and maximum 64 chars":
                alert("Nazwa użytkownika musi mieć minimum 4 i maksymalnie 64 znaki.")
                break;

              case "Username isn't available":
                alert("Nazwa użytkownika jest zajęta.")
                break;

              case "Username can contains only letters, digits, underscores and dotes.":
                alert("Nazwa użytkownika może zawierać tylko litery, cyfry, podkreślenia i kropki.")
                break;

              case "Email has to have maximum 64 chars":
                alert("Adres e-mail może mieć maksymalnie 64 znaki.");
                break;

              case "Email isn't correct":
                alert("Adres e-mail ma niepoprawny format.")
                break;

              case "Email isn't available":
                alert("Adres e-mail jest zajęty");
                break;

              case "First name has to have minimum 2 and maximum 32 chars":
                alert("Imię może mieć minimalnie 2 i maksymalnie 32 znaki.");
                break;

              case "First name contains only letters.":
                alert('Imię może zawierać tylko litery.');
                break;

              case "Last name has to have minimum 2 and maximum 32 chars":
                alert("Nazwisko może mieć minimalnie 2 i maksymalnie32 znaki.");
                break;

              case "Last name contains only letters and white spaces.":
                alert("Nazwisko może zawierać tylko litery, pauzy i spacje.")
                break;

              case "Choose right localization.":
                alert("Wybierz prawidłową lokalizację");
                break;

              case "City doesn't exist.":
                alert("Miasto nie istnieje.")
                break;

              case "Old password is invalid.":
                alert("Stare hasło jest nieprawidłowe.");
                break;
              case "Password needs to have minimum 6 and maximum 32 chars.":
                alert("Hasło może mieć od 6 do 32 znaków.");
                break;
              case "Password needs to have at least one small and big letter and one digit.":
                alert("Hasło musi zawierać małe i wielkie litery oraz cyfry.");
                break;

              default:
                alert("Wystąpił problem, spróbuj ponownie za chwilę");
                break;
            }
          }
        });
    },
    remove_friend(state, [form, fetch_info]) {
      let request = new Request(
        '/api/friends',
        {
          method: "DELETE",
          body: form,
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => handle_response(response))
        .then(data => {
          let index = state.user.friends.findIndex(f => {
            f.id == parseInt(form.get('friend_id'))
          });
          state.user.friends.splice(index, 1);
        })
        .catch(fetch_info);
    },
    fetch_invitations(state) {
      let request = new Request(
        "/api/invitation",
        {
          method: "GET",
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => response.json())
        .then(data => {
          state.invitations = data;
        })
    },
    send_invitation(state, [form, fetch_info]) {
      let request = new Request(
        '/api/invitation',
        {
          method: "POST",
          body: form,
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => handle_response(response))
        .then(data => {
          state.invitations.posted.push(data);
        })
        .catch(error => {
          if (error.message !== "NOT FOUND") fetch_info();
        });
    },
    accept_invitation(state, [invitation, el, type, fetch_info]) {
      /**
      * Accepts user invitation by sending GET request to server with id of
      * invitation.
      * @param { Object } invitation - object with info about invitation.
      * @param { Object } el - HTML element of invitation.
      */
      let request = new Request(
        "/api/accept_invitation/" + invitation.id,
        {
          method: "GET",
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => handle_response(response))
        .then(data => {
          remove_invitation_from_store(state, invitation, el, type);
          state.user.friends.push(invitation.sender);
        })
        .catch(() => {
          if (!!el) {
            remove_invitation_from_store(state, invitation, el, type);
            setTimeout(fetch_info, 800);
          }
          else {
            fetch_info();
          }
        });
    },
    delete_invitation(state, [invitation, el, type, fetch_info]) {
      /**
      * Rejects or deletes user invitation by sending DELETE request to server with
      * id of invitation.
      * @param { Object } invitation - object with info about invitation.
      * @param { Object } el - HTML element of invitation.
      */
      let request = new Request(
        "/api/invitation/" + invitation.id,
        {
          method: "DELETE",
          credentials: "same-origin"
        }
      );

      fetch(request)
        .then(response => handle_response(response))
        .then(data => {
          remove_invitation_from_store(state, invitation, type, el)
        })
        .catch(fetch_info);
    }
  },
  actions: {
    fetch_user(context, redirect) {
      context.commit('fetch_user', redirect);
    },
    update_user(context, form) {
      context.commit('update_user', form);
    },
    remove_friend(context, form) {
      context.commit('remove_friend', [form, fetch_info(context)]);
    },
    fetch_invitations(context) {
      context.commit('fetch_invitations');
    },
    send_invitation(context, form) {
      context.commit('send_invitation', [form, fetch_info(context)])
    },
    accept_invitation(context, data) {
      data.push(fetch_info(context));
      context.commit('accept_invitation', data);
    },
    delete_invitation(context, data) {
      data.push(fetch_info(context));
      context.commit('delete_invitation', data);
    }
  }
});

export default store;
