import Cockpit from "./components/cockpit/Cockpit.vue";
import Moments from "./components/moments/Moments.vue";
import Settings from "./components/settings/Settings.vue";
import Personalization from "./components/personalization/Personalization.vue";
import NewMoment from "./components/new_moment/NewMoment.vue";
import UserProfile from "./components/user/UserProfile.vue"
import UserMoment from "./components/user/UserMoment.vue";
import City from "./components/city/City.vue";

export default [
  {path: '/', component: Cockpit, name: "Kokpit", nav: true},
  {path: '/moments', component: Moments, name: "Momenty", nav: true},
  {path: '/settings', component: Settings, name: "Ustawienia", nav: true},
  {path: '/personalization', component: Personalization, name: "Personalizacja", nav: false},
  {path: '/add_moment', component: NewMoment, name: "Dodaj moment", nav: false},
  {path: '/user/:id', component: UserProfile, name:"Profil użytkownika", nav: false},
  {path: '/city/:id', component: City, name:"Profil miejsca", nav: false},
  {path: '/user_moments/:id', component: UserMoment, name:"Zobacz momenty", nav: false},
  {path: '*', component: Cockpit, name: "Strona nie istnieje", nav: false}
]
