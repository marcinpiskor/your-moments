import os
import re
import time
import requests
import unittest
from flask import Flask
from flask_testing import LiveServerTestCase
from app import create_app, db
from app.auth.models import User, Invitation, City
from app.main.models import Moment
from sqlalchemy import or_
from datetime import date, datetime
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from shutil import copyfile
from test_auth import TestMixin

class FunctionalTest(TestMixin, LiveServerTestCase):
    def create_app(self):
        app = create_app('live-test')
        app.config.update(LIVESERVER_PORT=5000)

        return app

    def setUp(self):
        self.app = self.create_app()
        db.create_all()
        path = self.app.config['GECKO']
        self.browser = webdriver.Firefox(executable_path=path)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.browser.quit()

    def create_basic_user(self):
        """
        Creates User object with basic data and returns that object and password
        """
        user = User(username="tester", email="tester@tester.tester")
        password = "awd123A"
        user.password = password
        user.generate_full_name()

        db.session.add(user)
        db.session.commit()

        return user, password

    def index_login(self, username, password):
        """
        Handles logging user on the page.

        Argument/s:
            username - username of User to authenticate
            password - password of User to authenticate
        """
        self.browser.get(self.get_server_url())
        time.sleep(2)

        login_input = self.browser.find_element_by_id('username')
        passwd_input = self.browser.find_element_by_id('password')

        login_input.send_keys(username)
        passwd_input.send_keys(password)

        self.browser.find_element_by_class_name('login-button').click()
        time.sleep(10)

    def test_successful_index_login(self):
        """
        Tests logging in on index page.
        User should be succesfully authenticated.
        """
        user, password = self.create_basic_user()
        self.index_login(user.username, password)

        self.assertEqual("Witaj, {}.".format(user.username),
                    self.browser.find_element_by_tag_name('h1').text)

    def test_unsuccessful_index_login(self):
        """
        Tests logging in on index page.
        Error with info about unsuccesful authentication should be displayed.
        """
        user, password = self.create_basic_user()
        self.index_login("wrong_username", password)

        err = "account_circleDane logowania są niepoprawne, spróbuj ponownie."
        self.assertEqual(err,
                self.browser.find_element_by_class_name('error-block').text)

    def google_auth(self):
        """
        Authenticates user on page via Google account.
        """
        self.browser.get("https://accounts.google.com/ServiceLogin")

        email = self.browser.find_element_by_css_selector("[type=email]")
        email.send_keys(self.app.config["GOOGLE_USERNAME"])

        submit = lambda x: self.browser.find_element_by_id(x).click()
        submit("identifierNext")

        time.sleep(10)

        password = self.browser.find_element_by_css_selector("[type=password]")
        password.send_keys(self.app.config["GOOGLE_PASSWORD"])

        submit("passwordNext")
        time.sleep(2)

        self.browser.get(self.get_server_url() + "/googleauth")
        time.sleep(10)

        #  Choosing Google account to authenticate
        self.browser.find_element_by_css_selector('li').click()
        time.sleep(10)

    def test_google_auth(self):
        """
        Tests authenticating user on page via Google account.
        User should be succesfully authenticated.
        """
        self.google_auth()
        self.assertEqual("Witaj, {}.".format(User.query.first().username),
                            self.browser.find_element_by_tag_name('h1').text)

    def twitter_auth(self):
        """
        Authenticates user on page via Twitter account.
        """
        self.browser.get('https://twitter.com/login')

        email = self.browser.find_element_by_class_name('js-username-field')
        email.send_keys(self.app.config["TWITTER_USERNAME"])
        password = self.browser.find_element_by_class_name('js-password-field')
        password.send_keys(self.app.config["TWITTER_PASSWORD"])

        password.send_keys(Keys.ENTER)

        time.sleep(5)

        self.browser.get(self.get_server_url() + "/twitterauth")
        time.sleep(10)

    def test_twitter_auth(self):
        """
        Tests authenticating user on page via Twitter account.
        User should be succesfully authenticated.
        """
        self.twitter_auth()
        self.assertEqual("Witaj, {}.".format(User.query.first().username),
                            self.browser.find_element_by_tag_name('h1').text)

    def test_social_auth(self):
        """
        Tests authenticating user on page via Google and Twitter account.
        User should be succesfully authenticated.
        """
        self.twitter_auth()
        primary_msg = self.browser.find_element_by_tag_name('h1').text

        self.browser.get(self.get_server_url() + "/logout")
        time.sleep(5)

        self.google_auth()
        # Welcome message after Twitter auth after should be equal to message
        # after Google auth.
        self.assertEqual(primary_msg,
                            self.browser.find_element_by_tag_name('h1').text)

    def test_aregistration(self):
        """
        Tests creating new user on register.
        Block with info about successful creating of new user should be
        displayed.
        """
        u = self.create_basic_user()[0]

        self.browser.get(self.get_server_url() + "/register")
        time.sleep(5)

        fields = [
            "email", "username", "first_name", "last_name", "birthdate",
            "localization", "password", "password_repeat"
        ]
        fields_el = {}

        for field in fields:
            name = "[name={}]".format(field)
            fields_el[field] = self.browser.find_element_by_css_selector(name)

        set_f = lambda x, y: fields_el[x].send_keys(y)

        # Setting value of fields to invalid values
        set_f('email', u.email)
        time.sleep(2)
        set_f('username', u.username)
        time.sleep(2)
        set_f('first_name', 'junior')
        set_f('last_name', 'da silva')
        set_f('birthdate', '1562-12-12')
        set_f('localization', 'Kurytyba')
        set_f('password', 'awd123A')
        set_f('password_repeat', 'awd123AB')

        time.sleep(5)

        # List with errors that should be displayed on page after submit of form
        # registration
        exp_errors = [
            "email Adres e-mail jest zajęty.",
            "account_circle Nazwa użytkownika jest zajęta.",
            'today Format daty jest niepoprawny',
        ]

        self.check_error_blocks(exp_errors)

        # Clearing value of all fields except first and last name
        clear_fields = lambda x: [fields_el[f].clear() for f in x]
        clear_fields(["email", "username", "birthdate"])

        # Setting value of fields to valid values
        set_f('email', "junior@da.silva")
        time.sleep(2)
        set_f('username', "junior123")
        time.sleep(2)
        set_f('birthdate', '1962-12-12')

        submit = self.browser.find_element_by_class_name('login-button')
        self.assertEqual(submit.text, "Zarejestruj się")

        submit.click()
        time.sleep(5)

        exp_errors = [
            "explore Wybierz poprawną lokalizację.",
            "security Hasła nie są takie same.",
            "check_box Zaznacz poniższe pole."
        ]

        self.check_error_blocks(exp_errors)

        clear_fields(["password", "password_repeat"])

        set_f('localization', Keys.DOWN)
        set_f('localization', Keys.RETURN)
        set_f('password', 'awd123A')
        set_f('password_repeat', 'awd123A')

        submit.click()

        # Accepting ReCAPTCHA
        time.sleep(30)
        submit.click()
        time.sleep(10)

        self.assertEqual("account_circleMożesz się zalogować.",
                self.browser.find_element_by_class_name("error-block").text)

    def check_error_blocks(self, exp_errors):
        """
        Checks if all expected errors has been displayed.

        Argument/s:
            exp_errors - list with errors that are expected to be displayed
        """
        errors = []

        for e in self.browser.find_elements_by_class_name('error-block'):
            errors.append(e.text)
        self.assertTrue(all([e in errors for e in exp_errors]))

    def test_logout(self):
        """
        Tests logging out of user.
        Block with info about successful logging out of user should be displayed
        """
        user, password = self.create_basic_user()
        self.index_login(user.username, password)
        time.sleep(5)

        self.browser.get(self.get_server_url() + "/logout")
        time.sleep(10)
        self.assertEqual("account_circleZostałeś pomyślnie wylogowany.",
                self.browser.find_element_by_class_name("error-block").text)

    def test_personalization(self):
        """
        Tests personalization component.
        All expected subcomponents (Background, Avatar, Color) should be
        properly displayed.
        """
        user, password = self.create_basic_user()
        self.index_login(user.username, password)

        submit = self.browser.find_element_by_class_name("skip-button")
        self.assertEqual(submit.text, "Dalej")

        # Testing personalization header content
        self.assertEqual(self.browser.find_element_by_tag_name('h1').text,
        'Witaj, {}.'.format(user.username))
        self.assertEqual(self.browser.find_element_by_tag_name('h2').text,
        'Konfigurujemy Twój profil')

        # Testing background personalization slide
        self.assertEqual(self.browser.find_element_by_tag_name('h3').text,
                                    'photo_size_select_actual Wybierz tło.')

        submit.click()
        time.sleep(5)

        # Testing avatar personalization slide
        self.assertEqual(self.browser.find_element_by_tag_name('h3').text,
                                    'photo_size_select_actual Wybierz awatar')

        submit.click()
        time.sleep(5)

        # Testing color personalization slide
        self.assertEqual(self.browser.find_element_by_tag_name('h3').text,
                                                        'Wybierz kolor tła')
        self.assertEqual(
            len(self.browser.find_elements_by_class_name('color-box')), 5
        )

        # Checking adding new color to color picker
        color = '#111abc'
        self.browser.find_element_by_tag_name('input').send_keys(color)
        self.browser.find_element_by_tag_name('button').click()

        self.assertEqual(
            len(self.browser.find_elements_by_class_name('color-box')), 6
        )

        get_active = lambda: self.browser.find_element_by_class_name('active')

        active = get_active()
        new_color = self.browser.find_elements_by_class_name('color-box')[-1]

        self.assertEqual(new_color.get_attribute('title'), color)

        # Active color should be changed after click of other color
        new_color.click()
        self.assertIn("active", new_color.get_attribute("class"))
        self.assertNotEqual(new_color, active)
        self.assertEqual(new_color, get_active())

    def handle_cockpit(self):
        """
        Creates basic User and City objects necessary to cockpit tests.
        """
        user_1, password = self.create_basic_user()

        city = City(name="Kartagena", region="Bolivar", country="Kolumbia",
                            place_id="ChIJUROdrucl9o4RyiY_Ay45YbE")
        db.session.add(city)
        db.session.commit()

        # Creating other users and friend
        user_2 = User(username="kitty", first_name="Kate", last_name="Walker",
                                        email="kate@mail.com", avatar="abd.png")
        user_3 = User(username="John", first_name="John", email="john@mail.com",
                                                    avatar="123.jpg", city=city)
        friend = User(username="Hector", email="hector@mail.com",
                                                avatar="avatar.png")
        # Preparing info about friend
        friend.generate_full_name()
        db.session.add(friend)
        db.session.commit()
        user_1.add_friend(friend)

        # Preparing info about other users and creating invitation related with
        # main user
        for u in [user_2, user_3]:
            u.generate_full_name()
            db.session.add(u)
            db.session.commit()
            db.session.add(Invitation(sender=u, invites=user_1))
            db.session.commit()

        self.index_login(user_1.username, password)

        # Skipping personalization mode by clicking button
        submit = self.browser.find_element_by_class_name("skip-button")
        [submit.click() for x in range(3)]

        return {"user_1": user_1, "users": [user_2, user_3], "friend": friend}

    def test_cockpit_invitations(self):
        """
        Test displaying invitations on cockpit page.
        Header values of invitation blocks should be proper.
        """
        users = self.handle_cockpit()["users"]
        invitations = self.browser.find_elements_by_class_name('invitation')

        self.check_user_component_correctness(users, invitations)

    def test_cockpit_accept_and_reject(self):
        """
        Test acceping and rejecting invitations on cockpit page.
        Heading values of invitation blocks should be proper.
        """
        self.handle_cockpit()
        invitations = self.browser.find_elements_by_class_name('invitation')

        self.assertEqual(len(invitations), 2)

        accepted_inv, rejected_inv = invitations

        # Extracting information from invitation to accept
        accepted_first_info = accepted_inv.find_element_by_tag_name('h3').text
        accepted_second_info = accepted_inv.find_element_by_tag_name('h4').text
        accepted_avatar = accepted_inv.find_element_by_class_name('avatar').get_attribute('style')

        accepted_inv.find_element_by_class_name('accept').click()
        time.sleep(3)

        # Getting last added friend
        new_friend = self.browser.find_elements_by_class_name('friend')[-1]

        # New friend should be added after accepting invitation
        self.assertEqual(accepted_first_info,
                                new_friend.find_element_by_tag_name('h3').text)
        self.assertEqual(accepted_second_info,
                                new_friend.find_element_by_tag_name('h4').text)
        self.assertEqual(accepted_avatar,
        new_friend.find_element_by_class_name('avatar').get_attribute('style'))

        amount_of_friends = len(self.browser.find_elements_by_class_name('friend'))

        rejected_inv.find_element_by_class_name('reject').click()
        time.sleep(3)

        self.assertEqual(0,
                len(self.browser.find_elements_by_class_name('invitation')))
        self.assertEqual(len(self.browser.find_elements_by_class_name('friend')),
                                                            amount_of_friends)

    def test_cockpit_search(self):
        """
        Test search bar on cockpit page.
        Proper results of each section that match the filter should be displayed
        """
        self.handle_cockpit()
        self.browser.find_element_by_class_name('container-button').click()

        search_bar = self.browser.find_element_by_css_selector('[type=search]')
        time.sleep(3)

        # Filter has less than 2 chars, so only 2 sections should be displayed
        search_bar.send_keys("k")
        self.assertEqual(
            len(self.browser.find_elements_by_class_name('list-component')), 2
        )

        # Filter has more than 2 chars, so 4 sections should be displayed
        search_bar.send_keys("a")
        self.assertEqual(
            len(self.browser.find_elements_by_class_name('list-component')), 4
        )

        time.sleep(5)

        city_el = self.browser.find_element_by_class_name("city")
        cities = City.query.filter(City.name.ilike("ka%")).all()
        self.assertEqual(len(cities), 1)

        # Checking if info about city object has been properly displayed and
        # background image of city block has been set to one of Google Place
        # photos
        self.assertEqual("{city.name}, {city.country}".format(city=cities[0]),
                            city_el.find_element_by_tag_name('h3').text)
        self.assertIn("googleusercontent.com", city_el.get_attribute('style'))

        users_obj = User.query.filter(or_(User.username.ilike("ka%"),
                                    User.full_name.ilike("ka%"))).all()
        invitations = self.browser.find_elements_by_class_name('invitation')
        users_and_friends = self.browser.find_elements_by_class_name('friend')

        for component in [invitations, users_and_friends]:
            self.check_user_component_correctness(users_obj, component)

    def test_cockpit_basic(self):
        """
        Test basic functionalities of cockpit page.
        Search bar should be visible on button click, section should be show and
        hide on section header click.
        """
        self.handle_cockpit()
        self.assertEqual(
            len(self.browser.find_elements_by_class_name('list-component')), 2
        )

        self.assertEqual(self.browser.find_element_by_tag_name('h1').text,
                                                                "YourMoments")

        cockpit_section = self.browser.find_element_by_class_name('list-component')
        cockpit_section.find_element_by_tag_name('header').click()

        # List should be hidden after click of header of it
        self.assertIn("list-hidden", cockpit_section.get_attribute('class'))
        self.assertEqual("max-height: 0px;",
            cockpit_section.find_element_by_class_name('list-content').get_attribute('style'))

        # List should be visible again after click
        cockpit_section.find_element_by_tag_name('header').click()
        self.assertNotEqual("max-height: 0px;",
            cockpit_section.find_element_by_class_name('list-content').get_attribute('style'))

        # Search bar should be visible after button click
        search_bar = self.browser.find_element_by_css_selector('[type=search]')
        self.assertNotIn("visible", search_bar.get_attribute('class'))

        search_btn = self.browser.find_element_by_class_name('container-button')
        search_btn.click()

        self.assertIn("visible", search_bar.get_attribute('class'))

    def test_cockpit_friends(self):
        """
        Test displaying friends on cockpit page.
        Heading values of friend blocks should be proper.
        """
        friends = self.handle_cockpit()["user_1"].friends.all()
        friends_blocks = self.browser.find_elements_by_class_name('friend')

        self.check_user_component_correctness(friends, friends_blocks)

    def test_moments_no_moments(self):
        """
        Test moments page when user has got friends but none of the set
        current user as viewer of moment.
        Info about it should be displayed.
        """
        self.handle_cockpit()
        self.browser.get(self.get_server_url() + "/home#moments")

        h1 = self.browser.find_elements_by_tag_name('h1')[-1]
        h2 = self.browser.find_element_by_tag_name('h2')
        info = "Żaden z twoich przyjaciół nie podzielił się momentem."

        self.assertEqual("sentiment_very_dissatisfied Brak momentów", h1.text)
        self.assertEqual(info, h2.text)

    def test_moments_no_friends(self):
        """
        Test moments page when user hasn't got friends.
        Info about it should be displayed.
        """
        self.handle_cockpit()
        self.remove_all_friends_of_current_user()

        self.browser.refresh()
        time.sleep(5)
        self.browser.get(self.get_server_url() + "/home#moments")

        h1 = self.browser.find_elements_by_tag_name('h1')[-1]
        h2 = self.browser.find_element_by_tag_name('h2')
        info = "Znajdź swoich przyjaciół, przejdź do Kokpitu."

        self.assertEqual("sentiment_very_dissatisfied Brak momentów", h1.text)
        self.assertEqual(info, h2.text)

    def test_moments_normal(self):
        """
        Test moments page when user has friends and moments where is set as
        viewer.
        All moments should be properly displayed.
        """
        self.handle_cockpit()
        self.generate_user_friends(8)

        self.browser.get(self.get_server_url() + "/home#moments")
        time.sleep(3)

        moments = [m.author for m in User.query.first().friends_moments.all()]
        hottest = moments.pop()

        hottest_el = self.browser.find_element_by_class_name('fresh')

        # Checking if paginating works
        load_button = self.browser.find_element_by_id('load-next')
        moments_el = self.browser.find_elements_by_class_name('moment')

        self.assertEqual("Załaduj kolejne", load_button.text)
        self.assertEqual(6, len(moments_el))

        load_button.click()
        time.sleep(5)
        moments_el = self.browser.find_elements_by_class_name('moment')

        self.assertEqual("Załadowano", load_button.text)
        self.assertEqual(7, len(moments_el))

        # Checking if hottest moment has been rendered properly
        get_text = lambda x, y: x.find_element_by_tag_name(y).text

        self.assertEqual(get_text(hottest_el, 'h2'), "whatshot Popularne")
        self.assertEqual(get_text(hottest_el, 'h3'), "@" + hottest.username)
        self.assertEqual(get_text(hottest_el, 'p'), "Przed chwilą")

        # Checking if moment has been rendered properly
        for i, obj in enumerate(moments):
            el = moments_el[i]

            self.assertEqual(get_text(el, 'h2'), "@" + obj.username)
            self.assertEqual(get_text(el, 'h3'), "Przed chwilą")
            self.assertEqual(get_text(el, 'h4'),
                                    "remove_red_eye " + str(obj.views))

    def test_add_moment_no_friends(self):
        """
        Test add moment when user hasn't got friends.
        User should be redirected to last page.
        """
        self.handle_cockpit()
        self.remove_all_friends_of_current_user()

        self.browser.refresh()
        exp_url = self.get_server_url() + "/home#moments"
        self.browser.get(exp_url)

        time.sleep(5)

        self.browser.find_element_by_class_name('add-moment-link').click()

        time.sleep(5)
        self.assertTrue(self.browser.current_url, exp_url)

    def test_add_moment_add_page(self):
        """
        Test add moment add content page.
        All heading texts should be displayed properly
        """
        self.handle_cockpit()
        self.browser.get(self.get_server_url() + "/home#moments")
        self.browser.find_element_by_class_name('add-moment-link').click()

        self.assertEqual(self.browser.find_element_by_tag_name('h1').text,
                                                            "Dodaj swój moment")
        self.assertEqual(self.browser.find_element_by_tag_name('h3').text,
                                                    "Wybierz zdjęcie lub film")

    def test_add_moment_customize_page(self):
        """
        Test add moment customize page.
        Customize menu and modals should be displayed properly.
        """
        self.handle_cockpit()
        self.browser.get(self.get_server_url() + "/home#moments")
        self.browser.find_element_by_class_name('add-moment-link').click()

        # Add photo
        time.sleep(15)

        self.assertEqual(self.browser.find_element_by_tag_name('h1').text,
                                                            "Dostosuj moment")

        # Checking if customize menu has been rendered properly
        exp_icons = ["brush", "timer", "blur_circular", "clear_all", "send"]
        icons = [i.text for i in self.browser.find_elements_by_tag_name("i")]

        for icon in exp_icons:
            self.assertIn(icon, icons)

        # Brush options
        self.check_basic_modal(0, "brush Pędzel",
                            ["Grubość pędzla", "Kolor pędzla", "Dodaj kolor"])

        select_color = Select(self.browser.find_element_by_tag_name('select'))
        current_color_val = select_color.first_selected_option.text

        # Adding incorrect color to modal colors
        add_color_field = self.browser.find_element_by_css_selector('[name=new_color]')
        add_color_field.send_keys('QWERTY')
        add_color_field.send_keys(Keys.ENTER)

        self.assertEqual(select_color.first_selected_option.text,
                                                        current_color_val)

        # Adding correct color to modal colors
        new_color = '#A1C'

        add_color_field.clear()
        add_color_field.send_keys(new_color)
        add_color_field.send_keys(Keys.ENTER)

        # Active color should be changed
        self.assertEqual(select_color.first_selected_option.text, new_color)

        colors_amount = lambda: len(self.browser.find_elements_by_tag_name('option'))
        current_amount_of_colors = colors_amount()

        add_color_field.send_keys(new_color)
        add_color_field.send_keys(Keys.ENTER)

        time.sleep(2)

        self.assertEqual(colors_amount(), current_amount_of_colors)

        self.check_close_modal("line_thick", "-2", "20")

        # Duration time
        self.check_basic_modal(self.get_option_index('Długość momentu')[1],
                            "timer Czas trwania", ["Długość momentu (w sek.)"])

        self.check_close_modal("duration", "0", "15")

        # Rubber options
        rubber, index = self.get_option_index("Opcje gumki")
        rubber.click()

        self.check_basic_modal(index, "blur_circular Gumka", ["Grubość gumki"])
        self.check_close_modal("rubber_thick", "-4", "5")


    def test_add_moment_send_page(self):
        self.handle_cockpit()
        self.browser.get(self.get_server_url() + "/home#moments")
        self.browser.find_element_by_class_name('add-moment-link').click()

        # Add photo
        time.sleep(15)

        # Click send moment button
        self.browser.find_element_by_css_selector("li:last-child").click()

        # Check all send moments options texts
        time.sleep(5)
        options = [o.text for o in self.browser.find_elements_by_class_name('option')]
        options = "".join(options)

        exp_options = [
            'check_box_outline_blank Dołącz informacje o lokalizacji',
            'people_outline Wyślij do:'
        ]

        for o in exp_options:
            self.assertIn(o, options)

        # Active geolocalizating
        moment_city = self.browser.find_element_by_class_name('option')
        moment_city.click()

        time.sleep(25)

        # Text of first option should be changed to name and country of city
        current_location = moment_city.text
        self.assertTrue(re.match(r"^check_box [a-zA-Z, ]+$", current_location))

        # Text of first option should be changed to previous value
        moment_city.click()
        self.assertEqual(moment_city.text, exp_options[0])

        # Text of first option should be changed to name and country of city
        moment_city.click()
        self.assertEqual(moment_city.text, current_location)

        # Checking if all current user friends has been displayd on viewers list
        friends = [u.username for u in User.query.first().friends.all()]
        viewers = [v.text for v in self.browser.find_elements_by_class_name('viewer')]

        for friend in friends:
            self.assertIn("check_box_outline_blank " + friend, viewers)

        test_viewer = self.browser.find_element_by_class_name('viewer')

        # Viewer after click should be checked
        test_viewer.click()
        self.assertTrue(test_viewer.text.startswith('check_box '))

        # Viewer after click should be unchecked
        test_viewer.click()
        self.assertTrue(test_viewer.text.startswith('check_box_outline_blank '))

        # Moment after send moment button click shouldn't be created because
        # viewers list is empty
        send_moment = self.browser.find_element_by_id('send-moment')
        send_moment.click()
        time.sleep(5)

        self.assertTrue(self.browser.current_url.endswith('#/add_moment'))

        # Moment after send moment button click should be created
        test_viewer.click()
        send_moment.click()
        time.sleep(5)

        self.assertTrue(self.browser.current_url.endswith('#/moments'))

    def test_user_moments_no_user(self):
        """
        Test user moments page when current user tries to reach moments of user
        that doesn't exist.
        User should be redirected to last page.
        """
        self.handle_cockpit()
        self.handle_user_moments_error(123)

    def test_user_moments_no_moments(self):
        """
        Test user moments page when current user tries to reach moments of user
        which is friend of current user but doesn't have moments related with
        current user.
        User should be redirected to last page.
        """
        self.handle_cockpit()
        self.handle_user_moments_error(User.query.first().friends.first().id)

    def test_user_moments_image(self):
        """
        Test user moments page where moment is image.
        Image and info about moment's author should be displayed.
        """
        self.handle_test_user_moments('1.jpg', 'img')

    def test_user_moments_video(self):
        """
        Test user moments page where moment is image.
        Video or codec's errors and info about moment's author should be
        displayed.
        """
        self.handle_test_user_moments('vid_valid.mp4', 'video')

    def test_user_moments_city(self):
        """
        Test user moments page where moment is related with city.
        Image info and about moment's author and location should be displayed.
        """
        self.handle_test_user_moments('1.jpg', 'img', city=True)

    def test_city_profile_exists(self):
        """
        Test city profile page.
        Info about city (photo, name, moments and users related) should be
        displayed properly.
        """
        self.handle_cockpit()
        self.handle_user_moments('1.jpg', True)

        city = City.query.first()
        self.browser.get(self.get_server_url() + "#/city/" + str(city.id))

        time.sleep(10)

        # Checking city background and name
        city_block = self.browser.find_element_by_class_name('city-back')
        self.assertEqual(city_block.find_element_by_tag_name('h1').text,
                                                                    city.name)
        self.assertIn("googleusercontent", city_block.get_attribute('style'))

        # Checking city components headers
        exp_comp_names = ["filter Powiązane momenty", "people Powiązani użytkownicy"]
        comp_names = [el.text for el in self.browser.find_elements_by_css_selector("header h2")]

        [self.assertIn(c, comp_names) for c in exp_comp_names]

        # Checking if all moments related with city has been rendered
        exp_moments = ["@" + m.author.username for m in city.moments.all()]
        moments = [el.text for el in self.browser.find_elements_by_class_name("moment-preview")]

        [self.assertIn(m, exp_moments) for m in exp_moments]

        self.check_users_on_profile([u.username for u in city.users.all()])

    def test_user_profile_normal(self):
        """
        Test user profile page of user that is not a friend of current user.
        All info about user should be displayed properly and button responsible
        for sending invatation should be rendered.
        """
        self.handle_cockpit()
        # Preparing user with proper info
        birthday = date(1990, 11, 11)

        u = User(first_name="Joao", last_name="Mario", city=City.query.first(),
                    birthdate=birthday, username="mariano123", avatar="a.jpg",
                                                        background="b.jpg")
        u.generate_full_name()
        db.session.add(u)
        db.session.commit()

        self.browser.refresh()
        self.browser.get(self.get_server_url() + "#/user/" + str(u.id))

        gen_url = lambda x: os.path.join(self.get_server_url(), 'static/upload', x)

        # Checking if avatar and background elements has been rendered
        background_el = self.browser.find_element_by_class_name('user-back')
        self.assertEqual(
            background_el.value_of_css_property('background-image'),
            'url("{}")'.format(gen_url(u.background))
        )

        avatar_el = self.browser.find_element_by_class_name('avatar')
        self.assertEqual(avatar_el.value_of_css_property('background-image'),
                                        'url("{}")'.format(gen_url(u.avatar)))

        self.assertEqual(self.browser.find_element_by_tag_name('h2').text,
                                                            "@" + u.username)

        # Checking if friends list component has been rendered
        self.assertIn("people Znajomi",
                self.browser.find_element_by_css_selector('header h2').text)

        # Setting expected age info
        d = datetime.now()
        age = str(int((date(d.year, d.month, d.day) - birthday).days / 365))
        age += " lat"

        if (age[-1] in ["2", "3", "4"]):
            age += "a"

        # Checking if all info about user has been rendered
        exp_info = [
            "person_pin " + u.full_name,
            "cake " + age,
            "remove_red_eye {} wyświetleń".format(str(u.views)),
            "location_on {city.name}, {city.country}".format(city=u.city)
        ]

        info_elements = self.browser.find_elements_by_css_selector("aside li")

        for i, info in enumerate(exp_info):
            self.assertEqual(info_elements[i].text, info)

        # Checking sending invitation to user
        friend_button = self.browser.find_element_by_class_name("user-button")
        self.assertEqual(friend_button.text, "person_add Dodaj znajomego")

        friend_button.click()
        time.sleep(3)

        # Checking deleting invitation to user
        self.assertEqual(friend_button.text, "cancel Anuluj zaproszenie")

        friend_button.click()
        time.sleep(3)

        self.assertEqual(friend_button.text, "person_add Dodaj znajomego")

    def test_user_profile_friend(self):
        """
        Test user profile page of user that is friend of current user.
        Friends of user and delete friend button should be displayed.
        """
        self.handle_cockpit()
        friend = User.query.first().friends.first()

        self.browser.get(self.get_server_url() + "#/user/" + str(friend.id))
        time.sleep(2)

        # Checking if friends has been displayed on user profile
        self.check_users_on_profile([u.username for u in friend.friends.all()])

        self.assertEqual(
            self.browser.find_element_by_class_name('moments').text,
            "Zobacz momenty"
        )

        # Checking deleting user from friends
        friend_button = self.browser.find_element_by_class_name("user-button")
        self.assertEqual(friend_button.text, "delete Usuń znajomego")

        friend_button.click()
        time.sleep(2)

        self.assertEqual(friend_button.text, "person_add Dodaj znajomego")

    def test_user_profile_accept_invitation(self):
        """
        Test user profile page of user that has sent invitation to current user.
        User should be added to friends of current user.
        """
        self.prepare_accept_and_reject()

        exp_button_texts = ["done Zaakceptuj", "clear Odrzuć"]
        button_texts = [el.text for el in self.browser.find_elements_by_tag_name("button")]

        [self.assertIn(b, button_texts) for b in exp_button_texts]

        self.browser.find_element_by_tag_name('button').click()
        time.sleep(2)

        self.assertIn(self.browser.find_element_by_tag_name("button").text,
                                                        "delete Usuń znajomego")

    def test_user_profile_reject_invitation(self):
        """
        Test user profile page of user that has sent invitation to current user.
        User shouldn't be added to friends of current user.
        """
        self.prepare_accept_and_reject()

        self.browser.find_elements_by_tag_name('button')[-1].click()
        time.sleep(2)

        self.assertIn(self.browser.find_element_by_tag_name("button").text,
                                                "person_add Dodaj znajomego")

    def test_settings(self):
        """
        Test settings page.
        Data of current user should be changed.
        """
        self.handle_cockpit()

        # Preparing data of current user
        current_user = User.query.first()
        current_user.first_name = "John"
        current_user.last_name = "Smith"
        current_user.avatar = "super_avatar123.jpg"
        current_user.background = "super_back456.png"
        current_user.city = City.query.first()

        db.session.add(current_user)
        db.session.commit()

        self.browser.refresh()
        self.browser.get(self.get_server_url() + "#/settings")

        time.sleep(5)

        head = self.browser.find_element_by_css_selector(".user-back h1").text
        self.assertEqual('Wybierz tło', head)

        # Check if buttons related with deleting of avatar and background have
        # been rendered.
        exp_buttons = [
            "photo_size_select_actual Usuń tło", "account_circle Usuń awatar"
        ]
        buttons_texts = []

        for del_button in self.browser.find_elements_by_class_name("file-del"):
            buttons_texts.append(del_button.text)
            del_button.click()

        [self.assertIn(b, buttons_texts) for b in exp_buttons]

        # After click of buttons related with modifying avatar and background,
        # buttons should be invisible and old background and avatar shouldn't
        # be visible.
        no_exist = lambda: self.browser.find_elements_by_class_name("file-del")
        self.assertRaises(no_exist)

        for class_name in ["user-back", "avatar"]:
            el = self.browser.find_element_by_class_name(class_name)
            self.assertEqual(el.value_of_css_property("background-image"),
                                                                        'none')

        # Checking if all fields has been filled with current values of user
        # fields
        fields = [
            "username", "email", "first_name", "last_name", "city", "color"
        ]
        fields_el = {}

        for field in fields:
            value = getattr(current_user, field)

            if field == "city":
                selector = '[name=localization]'
                el = self.browser.find_element_by_css_selector(selector)
                exp_city = "{city.name}, {city.country}".format(city=value)
                self.assertEqual(el.get_attribute('value'), exp_city)

            else:
                selector = "[name={}]".format(field)
                el = self.browser.find_element_by_css_selector(selector)
                self.assertEqual(el.get_attribute('value'), value)

            fields_el[field] = el

        submit = self.browser.find_element_by_css_selector('[type=submit]')
        self.assertEqual(submit.text, "Zapisz ustawienia")

        fields_el["username"].clear()
        fields_el["username"].send_keys(current_user.friends.first().username)

        # Error about username unavailability should be displayed.
        time.sleep(2)
        not_available = "Nazwa użytkownika i/lub adres e-mail są już zajęte."

        submit.click()
        self.check_and_accept_settings_alert(not_available)

        clear_fields = lambda x: [fields_el[f].clear() for f in x]

        [fields_el[f].clear() for f in ["username", "email"]]
        fields_el["username"].send_keys(current_user.username)
        fields_el["email"].send_keys(current_user.friends.first().email)

        # Error about email unavailability should be displayed.
        submit.click()
        self.check_and_accept_settings_alert(not_available)

        data = {
            "email": current_user.email,
            "color": "#abc123",
            "first_name": "Maria",
            "last_name": "Veracruz",
        }

        # Filling fields with correct data
        for field, value in data.items():
            fields_el[field].clear()
            fields_el[field].send_keys(value)

        submit.click()
        time.sleep(5)

        self.assertRaises(lambda: self.browser.switch_to.alert)

    def test_change_password(self):
        """
        Test change password form on settings page.
        Password of current user should be changed.
        """
        self.handle_cockpit()

        # Preparing current user to password change
        old_password = "iamOLDPASSWORD123"

        current_user = User.query.first()
        current_user.password = old_password
        db.session.add(current_user)
        db.session.commit()

        self.browser.refresh()
        self.browser.get(self.get_server_url() + "#/settings")

        data = {
            "old_password": old_password[:-1],
            "password": "awd123AB",
            "password_repeat": "awd123A"
        }

        fields_el = {}

        # Filling form fields with invalid values
        for field, value in data.items():
            selector = "[name={}]".format(field)
            el = self.browser.find_element_by_css_selector(selector)

            fields_el[field] = el
            el.send_keys(value)

        submit = self.browser.find_element_by_css_selector('.pass-form button')
        submit.click()

        self.check_and_accept_settings_alert("Hasła nie są takie same.")

        fields_el["password_repeat"].send_keys("B")
        submit.click()

        self.check_and_accept_settings_alert("Stare hasło jest nieprawidłowe.")

        fields_el["old_password"].send_keys(old_password[-1])
        submit.click()

        self.assertRaises(lambda: self.browser.switch_to.alert)

    def check_and_accept_settings_alert(self, error):
        """
        Checks if alert has been displayed and if text of it is proper

        Argument/s:
            error - expected content of alert
        """
        time.sleep(2)

        alert = self.browser.switch_to.alert
        self.assertEqual(alert.text, error)
        alert.accept()

    def check_users_on_profile(self, exp_users):
        """
        Check if all users has been displayed properly on city and user profile
        pages.

        Argument/s:
            exp_users - list with expected username of users
        """
        users = [f for f in self.browser.find_elements_by_class_name('friend')]

        for i, user in enumerate(exp_users):
            el = users[i]

            self.assertEqual(user, el.find_element_by_tag_name('h3').text)
            self.assertEqual("0 wyświetleń",
                                    el.find_element_by_tag_name('h4').text)


    def prepare_accept_and_reject(self):
        """
        Prepares browser to test accept and reject invitation test on user
        profile page.
        """
        self.handle_cockpit()

        current_user = User.query.first()
        user = Invitation.query.filter_by(invites=current_user).first().sender

        self.browser.get(self.get_server_url() + "#/user/" + str(user.id))
        time.sleep(2)

    def handle_test_user_moments(self, filename, moment_type, **kwargs):
        """
        Makes test of user moment page dependently on moment type (video or
        image).

        Argument/s:
            filename - filename of moment content
            moment_type - type of moment('video' or 'image')
        """
        self.handle_cockpit()
        moment = self.handle_user_moments(filename, **kwargs)

        self.browser.refresh()
        time.sleep(5)

        self.browser.get(self.get_server_url() + "/home#moments")
        time.sleep(10)

        org_url = self.browser.current_url
        self.browser.find_element_by_tag_name("button").click()

        time.sleep(3)

        self.assertEqual(self.browser.find_element_by_tag_name('h1').text,
                                                "@" + moment.author.username)

        exp_file = self.get_server_url() + "/static/upload/" + moment.content
        el = self.browser.find_element_by_tag_name(moment_type)
        self.assertEqual(el.get_attribute('src'), exp_file)

        if kwargs.get('city'):
            exp_city = "location_on {c.name}, {c.country}".format(c=moment.city)
            self.assertEqual(self.browser.find_element_by_tag_name('h2').text,
                                                                    exp_city)

        time.sleep(moment.duration)

        if moment_type == "video":
            try:
                alert = self.browser.switch_to.alert
                error = "Zainstaluj kodeki, by móc wyświetlać momenty typu wideo."
                self.assertEqual(alert.text, error)

            except NoAlertPresentException:
                self.assertEqual(self.browser.current_url, org_url)

    def handle_user_moments(self, filename, city=False):
        """
        Creates Moment object and copies moment file content to upload directory

        Argument/s:
            filename - name of file in 'images' directory to copy
            city - boolean that indicates if moment is related with City object
        """
        filepath = os.path.join(os.getcwd(), 'app/static/upload', filename)

        if not os.path.isfile(filepath):
            org_path = os.path.join(os.getcwd(), 'app/static/images', filename)
            copyfile(org_path, filepath)

        current_user = User.query.first()
        author = current_user.friends.first()
        moment = Moment(author=author, duration=20, content=filename)
        moment.viewers.append(current_user)

        if city:
            moment.city = City.query.first()

        db.session.add(moment)
        db.session.commit()

        return moment

    def handle_user_moments_error(self, user_id):
        """
        Makes tests of user moments page that is expected to display errors.

        Argument/s:
            user_id - id of User object
        """
        self.browser.get(self.get_server_url() + "/home#moments")
        current_url = self.browser.current_url

        time.sleep(2)

        self.browser.get(self.get_server_url() + "/home#user_moments/" + str(user_id))

        time.sleep(2)

        self.assertEqual(self.browser.current_url, current_url)

    def get_option_index(self, title):
        """
        Returns li elements with specific title and index of that element in
        list of all li elements.

        Argument/s:
            title - title of li element to get.
        """
        options = self.browser.find_elements_by_tag_name('li')
        option = self.browser.find_element_by_css_selector("[title='{}']".format(title))

        return option, options.index(option)

    def check_basic_modal(self, num, header_text, exp_labels):
        """
        Checks if customize modal has been displayed properly.

        Argument/s:
            num - index of li element on page
            header_text - expected header text of modal
            exp_labels - expected labels to display on modal
        """
        self.browser.find_elements_by_tag_name('li')[num].click()
        time.sleep(3)

        self.assertEqual(self.browser.find_element_by_tag_name('h3').text,
                                                                header_text)

        labs = [l.text for l in self.browser.find_elements_by_tag_name('label')]

        for label in exp_labels:
            self.assertIn(label, labs)

    def check_close_modal(self, name, wrong, proper):
        """
        Checks if modal closes.

        Argument/s:
            name - name of field which value of it indicates if modal can be
            closed
            wrong - invalid value of field
            proper - valid value of field
        """
        input_name = "[name={}]".format(name)

        input_field = self.browser.find_element_by_css_selector(input_name)
        input_field.clear()
        input_field.send_keys(wrong)

        modals_wrapper = self.browser.find_element_by_class_name("modals-wrapper")
        self.browser.find_element_by_tag_name('button').click()

        time.sleep(2)

        self.assertNotEqual(modals_wrapper.value_of_css_property("display"), "none")
        input_field.clear()
        input_field.send_keys(proper)
        input_field.send_keys(Keys.ENTER)

        try:
            self.browser.find_element_by_tag_name('button').click()

        except:
            pass

        time.sleep(2)

        self.assertEqual(modals_wrapper.value_of_css_property("display"), "none")

    def check_user_component_correctness(self, users, elements):
        """
        Checks if each user has been properly displayed.

        Argument/s:
            users - list with User objects to check
            elements - list with elements (invitations, friends, users etc.) to
            check
        """
        check_avatar = lambda x: 'background-image: url("{}");'.format(x)

        for u in users:
            el = next(self.get_elements(u, elements))
            self.assertTrue(el)

            self.assertEqual(
                el.find_element_by_class_name('avatar').get_attribute('style'),
                check_avatar("http://localhost:5000/static/upload/" + u.avatar)
            )

            h4 = el.find_element_by_tag_name("h4").text

            if u.city:
                self.assertEqual(h4, "{}, {}".format(u.city.name, u.city.country))
            else:
                self.assertEqual(h4, "0 wyświetleń")

    def get_elements(self, user, elements):
        """
        Generator method that get element that correspond to particular user.

        Argument/s:
            users - User object
            elements - list with elements (invitations, friends users etc.) to
            check
        """
        for el in elements:
            h3 = el.find_element_by_tag_name('h3').text

            if h3 == user.username or h3 == user.full_name:
                yield el

    def remove_all_friends_of_current_user(self):
        """
        Removes all friends from current user object.
        """
        current_user = User.query.first()
        current_user.friends = []
        db.session.add(current_user)
        db.session.commit()

    def generate_user_friends(self, amount):
        """
        Generates user friends and moments with user friend as author of them
        and current user as viewer of them.

        Argument/s:
            amount - amount of friends to generate
        """
        user = User.query.first()

        for i in range(amount):
            friend = User(username='friend_n_' + str(i), first_name="John",
                                                last_name="Popermann")
            friend.views = i
            db.session.add(friend)
            db.session.commit()

            user.add_friend(friend)

            moment = Moment(content='photo.jpg', author=friend, duration=20)
            moment.viewers.append(user)
            db.session.add(moment)
            db.session.commit()

if __name__ == "__main__":
    unittest.main()
