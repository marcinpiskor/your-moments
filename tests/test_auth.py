import os
import json
import random
import unittest
from io import BytesIO
from string import ascii_letters as letters
from datetime import datetime
from app import create_app, db
from app.config import BASE_DIR
from app.main.models import Moment
from app.auth.models import Role, User, City, Invitation
from app.auth.social_auth import (
    SocialAuth,
    GoogleSocialAuth,
    TwitterSocialAuth,
    authenticate_user,
    credentials_to_json,
    auth_dict
)
from auth.serializers import UserSerializer


class TestMixin:
    """
    Mixin for test cases of classes and functions that require access to db and
    working Flask app.
    """
    def setUp(self):
        self.app = create_app("test")
        self.client = self.app.test_client(use_cookies=True)
        self.app_context  = self.app.app_context()
        self.app_context.push()

        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def create_basic_user(self):
        """
        Creates basically configured user and returns dictionary with that user
        and valid password of it.
        """
        valid = "validpassword"
        u = User(username="tester", email="tester@tester.tester")
        u.password = "validpassword"

        return {"user": u, "valid": valid}

    def login_user(self):
        """
        Creates and authenticates user.
        """
        user_data = self.create_basic_user()
        self.user = user_data["user"]

        db.session.add(self.user)
        db.session.commit()

        data = {
            "username": self.user.username,
            'password': user_data["valid"]
        }

        self.client.post("/login", data=data)

    def handle_invitation_users(self):
        """
        Creates two users with purpose of testing friends resources.
        """
        user_2 = User(username="mari_b", first_name='Maria', last_name="Lopez")
        user_3 = User(username="m_pis", first_name="Marcin", last_name="Piskor")
        [db.session.add(x) for x in [user_2, user_3]]
        db.session.commit()

        return user_2, user_3

    def json(self, res):
        """
        Converts response data string to JSON format.

        Argument/s:
            res - Flask client response object.
        """
        return json.loads(res.get_data(as_text=True))


class RoleTestCase(TestMixin, unittest.TestCase):
    def init_roles(self):
        """
        Invokes Role static method responsible for creating basic roles (user,
        admin).
        Amount of Role objects is should be equal to 2.
        """
        Role.init_roles()
        self.assertEqual(Role.query.count(), 2)

    def test_init_roles(self):
        """
        Tests init roles Role static method.
        Only two Role objects should be created.
        """
        self.init_roles()
        self.init_roles()

    def test_default_role(self):
        """
        Tests roles created by init roles Role static method.
        User role should be marked as default role.
        """
        Role.init_roles()

        user_role = Role.query.filter_by(name="user").first()
        default_role = Role.query.filter_by(default=True).first()

        self.assertEqual(user_role, default_role)


class UserTestCase(TestMixin, unittest.TestCase):
    def test_password_getter(self):
        """
        Tests user's password property getter.
        Attribute error should be raised.
        """
        user = self.create_basic_user()["user"]
        self.assertRaises(AttributeError, lambda: user.password)

    def test_verify_password_valid(self):
        """
        Test user password verify method with valid data.
        Password should be verified.
        """
        user_data = self.create_basic_user()
        self.assertTrue(user_data["user"].verify_password(user_data["valid"]))

    def test_verify_password_invalid(self):
        """
        Tests user password verify method with invalid data.
        Password shouldn't be verified.
        """
        user = self.create_basic_user()["user"]
        self.assertFalse(user.verify_password("thisiswrongpassword123"))

    def test_check_availability_valid(self):
        """
        Tests user check availability of attributes method with valid data.
        Date of birth and e-mail address should be available.
        """
        self.assertTrue(User.check_availability("birthdate",
                                                        datetime.now().date()))
        self.assertTrue(User.check_availability("email", "valid@email.address"))

    def test_check_availability_invalid(self):
        """
        Tests user check availability of attributes method with invalid data.
        Date of birth and username shouldn't be available.
        """
        d = datetime.now().date()
        user = self.create_basic_user()["user"]
        user.birthdate = d
        db.session.add(user)

        self.assertFalse(User.check_availability("username", user.username))
        self.assertFalse(User.check_availability("birthdate", d))

    def test_check_availability_error(self):
        """
        Tests user check availability of attributes method with invalid
        attribute name.
        Attribute error should be raised.
        """
        self.assertRaises(AttributeError,
                        lambda: User.check_availability("wrongattr", "value"))

    def test_generate_full_name_first_last(self):
        """
        Tests generate full name method with first and last name included.
        Full name should be equal to first and last name combined with white
        space.
        """
        u = User(first_name='Martin', last_name="Da Silva")
        u.generate_full_name()

        self.assertEqual(u.full_name, "Martin Da Silva")

    def test_generate_full_name_first(self):
        """
        Tests generate full name method with first name included.
        Full name should be equal to first name.
        """
        u = User(first_name='Juninho')
        u.generate_full_name()

        self.assertEqual(u.full_name, "Juninho")

    def test_generate_full_name_last(self):
        """
        Tests generate full name method with last name included.
        Full name should be equal to last name.
        """
        u = User(last_name='Pernambucano')
        u.generate_full_name()

        self.assertEqual(u.full_name, "Pernambucano")

    def test_generate_full_name_blank(self):
        """
        Tests generate full name method with no names included.
        Full name should be empty.
        """
        u = User()
        u.generate_full_name()

        self.assertEqual(u.full_name, "")

    def test_add_friend_valid(self):
        """
        Tests add friend method with valid data.
        Friends should be added.
        """
        user, friend = self.handle_friends()

        self.assertIn(user, friend.friends.all())
        self.assertIn(friend, user.friends.all())

    def test_add_friend_already(self):
        """
        Tests add friend method with friend that has been already added to
        friends.
        Value error should be raised.
        """
        user, friend = self.handle_friends()


        self.assertRaises(ValueError, lambda: user.add_friend(friend))

    def test_add_friend_itself(self):
        """
        Tests add friend method on itself.
        Value error should be raised.
        """
        user = self.create_basic_user()["user"]
        self.assertRaises(ValueError, lambda: user.add_friend(user))

    def test_remove_friend_valid(self):
        """
        Tests remove friend method with valid data.
        Friends should be removed.
        """
        user, friend = self.handle_friends()

        user.remove_friend(friend)

        self.assertNotIn(user, friend.friends.all())
        self.assertNotIn(friend, user.friends.all())

    def test_remove_friend_invalid(self):
        """
        Tests remove friend method with user that is not a friend.
        Value error should be raised.
        """
        user = self.create_basic_user()["user"]
        friend = User(username="Maria")
        self.assertRaises(ValueError, lambda: user.remove_friend(friend))

    def handle_friends(self):
        """
        Creates two users, adds each other to friends and returns dict with
        those users.
        """
        user = self.create_basic_user()["user"]
        friend = User(username="Maria")
        db.session.add(friend)
        db.session.commit()

        user.add_friend(friend)

        return user, friend


class CityTestCase(TestMixin, unittest.TestCase):
    def test_check_if_exists_valid(self):
        """
        Tests city check if exists method with valid data.
        Tuple with true boolean value and dictionary with info about city should
        be returned.
        """
        valid_place_id = "ChIJz-fn0PHGPUcRcrIsc6p0wI0"
        exp_resp = (
            True,
            {
                'longitude': 21.1596321,
                'region': 'małopolskie',
                'latitude': 49.6546159,
                'name': 'Gorlice',
                'country': 'Polska'
            }
        )

        self.assertEqual(exp_resp, City.check_if_exists(valid_place_id))

    def test_check_if_exists_invalid(self):
        """
        Tests city check if exists method with invalid data.
        Tuple with false boolean value and empty dictionary should be returned.
        """
        wrong_id = [
            "not_valid_place", # Place with that id doesn't exist
            "ChIJDZURUqqMGpURULiMLaEKtMw" # This place id correspond to region
        ]

        exp_resp = (False, {})
        [self.assertEqual(exp_resp, City.check_if_exists(w)) for w in wrong_id]


class IndexTestCase(TestMixin, unittest.TestCase):
    def test_get(self):
        """
        Test GET request of index view.
        Slogan should be on page.
        """
        res = self.client.get("/")

        self.assertEqual(res.status_code, 200)
        self.assertIn("Uchwyć swoje <strong>#moments</strong>",
                                           res.get_data(as_text=True))


class LoginTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.user_data = self.create_basic_user()

        db.session.add(self.user_data["user"])
        db.session.commit()

    def test_get(self):
        """
        Test GET request of login view.
        Text about signing in should be on page.
        """
        res = self.client.get("/login")
        self.assertIn("Zaloguj się", res.get_data(as_text=True))

    def test_post_valid_username(self):
        """
        Test POST request of login view with valid data (username).
        User should be authenticated and redirected to home page.
        """
        data = {
            "data": {
                "username": self.user_data["user"].username,
                "password": self.user_data["valid"]
            },
            "follow_redirects": True,
            "content_type": "application/x-www-form-urlencoded"
        }

        res = self.client.post("/login", **data)
        self.assertIn("Strona główna", res.get_data(as_text=True))

    def test_post_valid_email(self):
        """
        Test POST request of login view with valid data (email).
        User should be authenticated and redirected to home page.
        """
        data = {
            "data": {
                "username": self.user_data["user"].email,
                "password": self.user_data["valid"]
            },
            "follow_redirects": True,
            "content_type": "application/x-www-form-urlencoded"
        }

        res = self.client.post("/login", **data)
        self.assertIn("Strona główna", res.get_data(as_text=True))

    def test_post_invalid_username(self):
        """
        Test POST request of login view with invalid data (username).
        Text about incorrect credentials should be displayed.
        """
        data = {
            "data": {
                "username": "invalid_username",
                "password": self.user_data["valid"]
            },
            "follow_redirects": True,
            "content_type": "application/x-www-form-urlencoded"
        }

        res = self.client.post("/login", **data)
        self.assertIn("Dane logowania są niepoprawne",
                                            res.get_data(as_text=True))

    def test_post_invalid_email(self):
        """
        Test POST request of login view with invalid data (email).
        Text about incorrect credentials should be displayed.
        """
        data = {
            "data": {
                "username": "invalid@email.com",
                "password": self.user_data["valid"]
            },
            "follow_redirects": True,
            "content_type": "application/x-www-form-urlencoded"
        }

        res = self.client.post("/login", **data)
        self.assertIn("Dane logowania są niepoprawne",
                                            res.get_data(as_text=True))

    def test_post_invalid_password(self):
        """
        Test POST request of login view with invalid data (password).
        Text about incorrect credentials should be displayed.
        """
        data = {
            "data": {
                "username": self.user_data["user"].username,
                "password": "invalid_password"
            },
            "follow_redirects": True,
            "content_type": "application/x-www-form-urlencoded"
        }

        res = self.client.post("/login", **data)
        self.assertIn("Dane logowania są niepoprawne",
                                            res.get_data(as_text=True))


class RegisterTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.user_data = self.create_basic_user()

        db.session.add(self.user_data["user"])
        db.session.commit()

    def test_get(self):
        """
        Test GET request of register view.
        Text about signing up should be on page.
        """
        res = self.client.get("/register")
        self.assertIn("Zarejestruj się", res.get_data(as_text=True))


class LogoutTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

    def test_logout(self):
        """
        Test logout view.
        Info about logging out should be displayed.
        """
        res = self.client.get("/logout", follow_redirects=True)
        self.assertIn("Zostałeś pomyślnie wylogowany.",
                                                    res.get_data(as_text=True))


class CheckUserDataTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/check_user_data"
        self.user_data = self.create_basic_user()

        self.user_data["user"].birthdate = datetime(2000, 2, 2)
        db.session.add(self.user_data["user"])
        db.session.commit()

    def test_available_attribute(self):
        """
        Tests POST check user data view with available data.
        All attributes should be marked as available.
        """
        available_data = [
            {"attr_name": "username", "value": "availableusername123"},
            {"attr_name": "email", "value": "available@email.com"},
            {"attr_name": "birthdate", "value": "1990-08-08"}
        ]

        for data in available_data:
            res = self.client.post("/check_user_data", data=json.dumps(data),
                                            content_type="application/json")
            self.assertTrue(self.json(res)["available"])

    def test_unavailable_attribute(self):
        """
        Tests POST check user data view with unavailable data.
        All attributes should be marked as unavailable.
        """
        available_data = ["username", "email", "birthdate"]

        for field in available_data:
            data = {
                "attr_name": field,
                "value": getattr(self.user_data["user"], field)
            }

            if field == "birthdate":
                data["value"] = data["value"].strftime("%Y-%m-%d")

            res = self.client.post("/check_user_data", data=json.dumps(data),
                                                content_type="application/json")
            self.assertFalse(self.json(res)["available"])

    def test_invalid_attribute(self):
        """
        Tests POST check user data view with invalid data (attribute name).
        Error about the lack of attribute in User model should be returned.
        """
        data = {"attr_name": "favorite_color", "value": "white"}
        exp_res = {"attr": "User model doesn't have attribute {}.".format
                                                        (data["attr_name"])}

        res = self.client.post("/check_user_data", data=json.dumps(data),
                                            content_type="application/json")
        self.assertEqual(self.json(res), exp_res)

    def test_invalid_birthdate(self):
        """
        Tests POST check user data view with invalid data (birhdate).
        Error about the wrong birthdate format should be returned.
        """
        data = {"attr_name": "birthdate", "value": "12/12/1994"}
        exp_res =  {
            "user": {
                "birthdate": "Birthdate must have format YYYY-MM-DD."
            }
        }

        res = self.client.post("/check_user_data", data=json.dumps(data),
                                            content_type="application/json")
        self.assertEqual(self.json(res), exp_res)


class SocialAuthTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.social = SocialAuth({})
        self.user_data = self.create_basic_user()

        db.session.add(self.user_data["user"])
        db.session.commit()

    def test_check_username_basic(self):
        """
        Test social auth check username method with available data.
        Basic username should be returned.
        """
        username_info = {
            "basic": "the_best_corleone123",
            "alter": "Michael Corleone"
        }

        self.assertEqual(self.social.check_username(username_info),
                                                    username_info["basic"])

    def test_check_username_available_alter(self):
        """
        Test social auth check username method with partially available data
        (alter).
        Formatted version of alter username should be returned.
        """
        username_info = {
            "basic": self.user_data["user"].username,
            "alter": "Michael Corleone"
        }

        self.assertEqual(self.social.check_username(username_info),
                                                            "Michael.Corleone")

    def test_check_username_available_alter_used(self):
        """
        Test social auth check username method with unavailable data.
        Formatted version of alter username with number should be returned.
        """
        db.session.add(User(username="Michael.Corleone"))
        db.session.commit()

        username_info = {
            "basic": self.user_data["user"].username,
            "alter": "Michael Corleone"
        }

        self.assertEqual(self.social.check_username(username_info),
                                                        "Michael.Corleone1")

    def test_check_username_available_max_len(self):
        """
        Tests social auth check username method with unavailable data.
        Formatted version of alter username with number that replacing last char
        should be returned.
        """
        username = "".join(
        [random.choice(letters) for i in range(self.social.max_len["username"])]
        )

        db.session.add(User(username=username))
        db.session.commit()

        username_info = {
            "basic": self.user_data["user"].username,
            "alter": username
        }

        self.assertEqual(self.social.check_username(username_info),
                                                    username[:-1] + "1")

    def test_format_username(self):
        """
        Tests social auth format username method.
        Formatted version of username (with dots instead unallowed chars) should
        be returned.
        """
        username = "Johnny |||| 11 Hands #@#$%^"
        self.assertEqual(self.social.format_username(username),
                                                        "Johnny.11.Hands")


class SocialAuthMixin:
    def test_authenticate_user_new(self):
        """
        Tests authenticate user function of particular social auth class.
        New user should be created.
        """
        social_data = self.auth_class(self.auth_data).data
        user = authenticate_user(self.auth_name, self.auth_data)

        for attr in social_data:
            self.assertEqual(getattr(user, attr), social_data[attr])

        self.assertEqual(User.query.count(), 1)

    def test_authenticate_user_new_already_used_username(self):
        """
        Tests authenticate user function of particular social auth class with
        already used username.
        New user with alter username should be created.
        """
        db.session.add(User(username=self.auth_username))
        social_data = self.auth_class(self.auth_data).data
        user = authenticate_user(self.auth_name, self.auth_data)

        self.assertEqual(social_data.pop("username")[:4], user.username[:4])

        for attr in social_data:
            self.assertEqual(getattr(user, attr), social_data[attr])

        self.assertEqual(User.query.count(), 2)

    def test_authenticate_user_already_registered_email(self):
        """
        Tests authenticate user function of particular social auth class with
        already used e-mail address.
        User should be connected with social service (id field).
        """
        social_data = self.auth_class(self.auth_data).data
        social_id = social_data.pop(self.auth_name + "_id")

        db.session.add(User(**social_data))
        db.session.commit()

        user = authenticate_user(self.auth_name, self.auth_data)

        self.assertEqual(User.query.count(), 1)
        self.assertEqual(getattr(user, self.auth_name + "_id"), social_id)

    def test_authenticate_user_already_created(self):
        """
        Tests authenticate user function of particular social auth class with
        already authenticated social account.
        User should be authenticated.
        """
        user = User(**self.auth_class(self.auth_data).data)
        db.session.add(user)
        db.session.commit()

        self.assertEqual(user, authenticate_user(self.auth_name, self.auth_data))
        self.assertEqual(User.query.count(), 1)


class GoogleSocialAuthTestCase(TestMixin, SocialAuthMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.auth_name = "google"
        self.auth_data = {
            'image': {
                'url': 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50',
                'isDefault': True
            },
            'kind': 'plus#person',
            'objectType': 'person',
            'language': 'pl',
            'nickname': 'paweloserw',
            'ageRange': {'min': 21},
            'name': {'familyName': '', 'givenName': 'Paweł'},
            'verified': False,
            'displayName': 'Paweł',
            'url': 'https://plus.google.com/108541310430682779190',
            'circledByCount': 0,
            'etag': '"FT7X6cYw9BSnPtIywEFNNGVVdio/w4Z4gcoO2p0IDpZvsPk2YOroBNw"',
            'emails': [
                {'value': 'pawelos.kosinos@gmail.com', 'type': 'account'}
            ],
            'birthday': '1990-03-19',
            'id': '108541310430682779190',
            'isPlusUser': True
        }
        self.auth_username = self.auth_data["nickname"]
        self.auth_class = GoogleSocialAuth

    def test_get_birthdate_normal(self):
        """
        Tests Google social auth get birthdate method with normal data (
        stringified date in format YYYY-MM-DD).
        Date object should be returned.
        """
        data = {"birthday": "1990-09-09"}

        self.assertEqual(GoogleSocialAuth.get_birthdate(data),
                    datetime.strptime(data["birthday"], "%Y-%m-%d"))

    def test_get_birthdate_age_range(self):
        """
        Tests Google social auth get birthdate method with range data (min and
        max range of user age).
        Date object with day and month setted to 1 should be returned.
        """
        data = {"ageRange": {"min": 21}}
        exp_res = datetime(datetime.now().year - data["ageRange"]["min"], 1, 1)

        self.assertEqual(GoogleSocialAuth.get_birthdate(data), exp_res)

    def test_get_birthdate_age_none(self):
        """
        Tests Google social auth get birthdate method with no data (empty dict).
        None value should be returned.
        """
        self.assertEqual(GoogleSocialAuth.get_birthdate({}), None)

    def test_get_email_valid(self):
        """
        Tests Google social auth get email method with valid data.
        E-mail address should be returned.
        """
        data = {
            'emails': [
                {'type': 'account', 'value': 'pawelos.kosinos@gmail.com'}
            ]
        }

        self.assertEqual(GoogleSocialAuth.get_email(data),
                                                data["emails"][0]["value"])

    def test_get_email_invalid(self):
        """
        Tests Google social auth get email with invalid data.
        StopIteration exception should be raised.
        """
        data = {
            'emails': [
                {'type': 'other', 'value': 'pawelos.kosinos@gmail.com'}
            ]
        }

        self.assertRaises(StopIteration,
                                    lambda: GoogleSocialAuth.get_email(data))

    def test_prepare_data(self):
        """
        Tests Google social auth prepare data.
        Dictionary with necessary info about user should be returned.
        """
        exp_res = {
            'username': 'paweloserw',
            'google_id': '108541310430682779190',
            'last_name': '',
            'birthdate': datetime(1990, 3, 19, 0, 0).date(),
            'email': 'pawelos.kosinos@gmail.com',
            'first_name': 'Paweł'
        }

        social = GoogleSocialAuth(self.auth_data)
        self.assertEqual(social.data, exp_res)


class TwitterSocialAuthTestCase(TestMixin, SocialAuthMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.auth_name = "twitter"
        self.auth_data = {
            'profile_sidebar_fill_color': '000000',
            'profile_background_image_url_https': 'https://abs.twimg.com/images/themes/theme1/bg.png',
            'profile_use_background_image': False,
            'profile_background_color': '000000',
            'profile_background_image_url': 'http://abs.twimg.com/images/themes/theme1/bg.png',
            'lang': 'pl',
            'screen_name': 'm_piskor',
            'entities': {'description': {'urls': []}},
            'favourites_count': 31,
            'followers_count': 0,
            'is_translator': False,
            'profile_background_tile': False,
            'profile_text_color': '000000',
            'profile_link_color': '1B95E0',
            'utc_offset': None,
            'friends_count': 196,
            'follow_request_sent': False,
            'id': 3350518041,
            'verified': False,
            'time_zone': None,
            'description': '',
            'default_profile': False,
            'url': None,
            'is_translation_enabled': False,
            'statuses_count': 0,
            'profile_image_url_https': 'https://abs.twimg.com/sticky/default_profile_images/default_profile_3_normal.png',
            'default_profile_image': True,
            'geo_enabled': False,
            'profile_banner_url': 'https://pbs.twimg.com/profile_banners/3350518041/1488562726',
            'notifications': False,
            'profile_sidebar_border_color': '000000',
            'id_str': '3350518041',
            'listed_count': 0,
            'has_extended_profile': False,
            'following': False,
            'email': 'marcin.piskor1@gmail.com',
            'created_at': 'Mon Jun 29 10:06:07 +0000 2015',
            'contributors_enabled': False,
            'location': 'Polska',
            'name': 'Marcin Piskor',
            'profile_image_url': 'http://abs.twimg.com/sticky/default_profile_images/default_profile_3_normal.png',
            'protected': False,
            'translator_type': 'none'
        }
        self.auth_username = self.auth_data["screen_name"]
        self.auth_class = TwitterSocialAuth

    def test_get_names_normal(self):
        """
        Tests Twitter social auth get names with normal data (name with
        multiple words separeted with blankspace).
        Tuple with first and last name should be returned.
        """
        exp_response = "Marcin", "Piskor"
        self.assertEqual(TwitterSocialAuth.get_names(self.auth_data),
                                                            exp_response)

    def test_get_names_single_word(self):
        """
        Tests Twitter social auth get names with single data (name that contains
        only one word).
        Tuple with two empty strings should be returned.
        """
        data = {"name": "MarcinPiskor"}
        exp_response = "", ""
        self.assertEqual(TwitterSocialAuth.get_names(data), exp_response)

    def test_get_email_valid(self):
        """
        Tests Twitter social auth get email with valid data.
        E-mail address should be returned.
        """
        self.assertEqual(TwitterSocialAuth.get_email(self.auth_data),
                                                    self.auth_data["email"])

    def test_get_email_invalid(self):
        """
        Tests Twitter social auth get email with invalid data.
        E-mail address should be returned.
        """
        data = {"email": ""}
        self.assertRaises(ValueError, lambda: TwitterSocialAuth.get_email(data))

    def test_prepare_data(self):
        """
        Tests Twitter social auth prepare data.
        Dictionary with necessary info about user should be returned.
        """
        exp_res = {
            'twitter_id': '3350518041',
            'email': 'marcin.piskor1@gmail.com',
            'first_name': 'Marcin',
            'username': 'm_piskor',
            'last_name': 'Piskor'
        }

        social = TwitterSocialAuth(self.auth_data)
        self.assertEqual(social.data, exp_res)


class CredentialsTestCase(TestMixin, unittest.TestCase):
    def test_credentials_to_json_valid(self):
        """
        Tests credentials to JSON function.
        Dictionary with credentials should be returned.
        """
        data = "oauth_token=3350518041-u1S5XdcAsYgwsr5U4RUOtRFsvY3TaJ6Jbn1zMee&oauth_token_secret=iSBb5FgE0MnxK1QdrSS3Ff5jUElpPQKpr4R3bEtOYOvMc&user_id=3350518041&screen_name=m_piskor&x_auth_expires=0"
        exp_res = {
            'x_auth_expires': '0',
            'user_id': '3350518041',
            'screen_name': 'm_piskor',
            'oauth_token_secret': 'iSBb5FgE0MnxK1QdrSS3Ff5jUElpPQKpr4R3bEtOYOvMc',
            'oauth_token': '3350518041-u1S5XdcAsYgwsr5U4RUOtRFsvY3TaJ6Jbn1zMee'
        }

        self.assertEqual(credentials_to_json(data), exp_res)

    def test_credentials_to_json_invalid(self):
        """
        Tests credentials to JSON function.
        Value error should be returned.
        """
        data = "errors=invalid_data"

        self.assertRaises(ValueError, lambda: credentials_to_json(data))


class UserListResourceTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.valid_data = {
            "email": "junior@dasilva.gmail",
            "username": "test",
            "first_name": "Junior",
            "last_name": "Da Silva",
            "city": "ChIJz-fn0PHGPUcRcrIsc6p0wI0",
            "birthdate": "1989-06-04",
            "password": "superSecretPassword123",
            "background": (BytesIO(b"data"), "back.jpg"),
            "avatar": (BytesIO(b"data"), "avatar.png"),
            "color": "#abde"
        }

    def tearDown(self):
        super().tearDown()
        to_delete = ["avatar", "back"]

        for filename in os.listdir(self.app.config["UPLOAD"]):
            for d in to_delete:
                if d in filename:
                    os.remove(os.path.join(self.app.config["UPLOAD"], filename))

    def test_get(self):
        """
        Test GET request of User resource.
        Serialized users should be returned
        """
        user1 = self.create_basic_user()["user"]
        user2 = User(username="mark123", email="mark@g.mail", color="#000000",
            background="cat.jpg", first_name="Mark", last_name="Johnson",
                                        birthdate=datetime(1990,12,12))
        user2.generate_full_name()
        city = City(name="New York", region="New York", country="USA")

        [db.session.add(x) for x in [user2, city]]
        db.session.commit()

        user2.city = city
        user2.add_friend(user1)

        exp_res = [
            {
                'color': '#000000',
                'friends': [
                    {
                        'color': None,
                        'username': 'tester',
                        'avatar': None,
                        'city': None,
                        'views': 0,
                        'id': 2,
                        'birthdate': None,
                        'first_name': None,
                        'full_name': None,
                        'last_name': None,
                        'background': None,
                        'email': 'tester@tester.tester'
                    }
                ],
                'username': 'mark123',
                'avatar': None,
                'city': {
                    'name': 'New York',
                    'id': 1,
                    'latitude': None,
                    'region': 'New York',
                    'country': 'USA',
                    'longitude': None,
                    'place_id': None
                },
                'views': 0,
                'id': 1,
                'birthdate': '1990-12-12',
                'first_name': 'Mark',
                'full_name': 'Mark Johnson',
                'last_name': 'Johnson',
                'background': 'http://localhost/static/upload/cat.jpg',
                'email': 'mark@g.mail'
            },
            {
                'color': None,
                'friends': [
                    {
                        'color': '#000000',
                        'username': 'mark123',
                        'avatar': None,
                        'city': {
                            'name': 'New York',
                            'id': 1,
                            'latitude': None,
                            'region': 'New York',
                            'country': 'USA',
                            'longitude': None,
                            'place_id': None
                        },
                        'views': 0,
                        'id': 1,
                        'birthdate': '1990-12-12',
                        'first_name': 'Mark',
                        'full_name': 'Mark Johnson',
                        'last_name': 'Johnson',
                        'background': 'http://localhost/static/upload/cat.jpg',
                        'email': 'mark@g.mail'
                    }
                ],
                'username': 'tester',
                'avatar': None,
                'city': None,
                'views': 0,
                'id': 2,
                'birthdate': None,
                'first_name': None,
                'full_name': None,
                'last_name': None,
                'background': None,
                'email': 'tester@tester.tester'
            }
        ]

        self.assertEqual(exp_res, self.json(self.client.get("/api/user")))

    def test_post_valid(self):
        """
        Test POST request of User resource with valid data.
        Serialized user should be returned.
        """
        exp_res = {
            "friends": [],
            "username": "test",
            "views": 0,
            "id": 1,
            "last_name": "Da Silva",
            "email": "junior@dasilva.gmail",
            "first_name": "Junior",
            "birthdate": "1989-06-04",
            "city": {
                "name": "Gorlice",
                "place_id": "ChIJz-fn0PHGPUcRcrIsc6p0wI0",
                "region": "małopolskie",
                "latitude": 49.6546159,
                "longitude": 21.1596321,
                "country": "Polska",
                "id": 1
            },
            "full_name": "Junior Da Silva",
            "background": "http://localhost/static/upload/back.jpg",
            "avatar": "http://localhost/static/upload/avatar.png",
            "color": "#abde"
        }

        self.make_test_request(exp_res)
        [self.assertEqual(x.query.count(), 1) for x in [User, City]]

    def test_post_email_invalid(self):
        """
        Test POST request of User resource with invalid username.
        Error should be returned.
        """
        self.valid_data["email"] = "thisisnotproper@email"
        exp_res = {'user': {'email': "Email isn't correct."}}

        res = self.client.post("/api/user", data=self.valid_data)

        self.assertEqual(exp_res, self.json(res))

    def test_post_email_already_used(self):
        """
        Test POST request of User resource with already used email.
        Error should be returned.
        """
        user = self.create_basic_user()["user"]
        db.session.add(user)
        db.session.commit()

        self.valid_data["email"] = user.email
        exp_res = {"user": {"email": "Email isn't available."}}

        res = self.client.post("/api/user", data=self.valid_data)

        self.assertEqual(exp_res, self.json(res))

    def test_post_username_invalid(self):
        """
        Test POST request of User resource with invalid username.
        Error should be returned.
        """
        self.valid_data["username"] = "invalid%#@--usenrame  "
        exp_res = {"user": {"username": "Username can contains only letters, digits, underscores and dotes."}}

        self.make_test_request(exp_res)

    def test_post_username_invalid_len(self):
        """
        Test POST request of User resource with invalid username.
        Error should be returned.
        """
        self.valid_data["username"] = "ab"
        exp_res = {"user": {"username": "Username has to have minimum 4 and maximum 64 chars"}}

        self.make_test_request(exp_res)

    def test_post_username_already_used(self):
        """
        Test POST request of User resource with username that is already used.
        Error should be returned.
        """
        user = self.create_basic_user()["user"]
        db.session.add(user)
        db.session.commit()

        self.valid_data["username"] = user.username
        exp_res = {"user": {"username": "Username isn't available"}}

        self.make_test_request(exp_res)

    def test_post_first_name_invalid(self):
        """
        Test POST request of User resource with invalid first name.
        Error should be returned.
        """
        self.valid_data["first_name"] = "Maria Teresa"
        exp_res = {"user": {"first_name": "First name contains only letters."}}

        self.make_test_request(exp_res)

    def test_post_first_name_invalid_len(self):
        """
        Test POST request of User resource with first name with invalid length.
        Error should be returned.
        """
        self.valid_data["first_name"] = "M"
        exp_res = {"user": {"first_name": "First name has to have minimum 2 and maximum 32 chars"}}

        self.make_test_request(exp_res)

    def test_post_last_name_invalid(self):
        """
        Test POST request of User resource with invalid last name.
        Error should be returned.
        """
        self.valid_data["last_name"] = "Cucaracho Parias123"
        exp_res = {"user": {"last_name": "Last name contains only letters and white spaces."}}

        self.make_test_request(exp_res)

    def test_post_first_name_invalid_len(self):
        """
        Test POST request of User resource with last name with invalid length.
        Error should be returned.
        """
        self.valid_data["last_name"] = "P"
        exp_res = {"user": {"last_name": "Last name has to have minimum 2 and maximum 32 chars"}}

        self.make_test_request(exp_res)

    def test_post_birdthdate_invalid_format(self):
        """
        Test POST request of User resource with invalid birthdate value (wrong
        format).
        Error should be returned.
        """
        self.valid_data["birthdate"] = "10/10/2000"
        exp_res = {"user": {"birthdate": 'Birthdate has to have format YYYY-MM-DD.'}}

        self.make_test_request(exp_res)

    def test_post_birdthdate_invalid_age(self):
        """
        Test POST request of User resource with invalid birthdate value (too
        big age).
        Error should be returned.
        """
        self.valid_data["birthdate"] = "1889-01-20"
        exp_res = {"user": {"birthdate": 'Birthdate is invalid.'}}

        self.make_test_request(exp_res)

    def test_post_city_blank(self):
        """
        Test POST request of User resource with blank city value.
        Error should be returned.
        """
        self.valid_data["city"] = ""
        exp_res = {"user": {"city": "Choose right localization."}}

        self.make_test_request(exp_res)

    def test_post_city_invalid(self):
        """
        Test POST request of User resource with invalid city (place_id) value.
        Error should be returned.
        """
        self.valid_data["city"] = "abcDQ210#@8912"
        exp_res = {"user": {"city": "City doesn't exist."}}

        self.make_test_request(exp_res)

    def test_post_password_invalid(self):
        """
        Test POST request of User resource with invalid password value.
        Error should be returned.
        """
        self.valid_data["password"] = "abcdef"
        exp_res = {"user": {"password": "Password needs to have at least one small and big letter and one digit."}}

        self.make_test_request(exp_res)

    def test_post_color_invalid(self):
        """
        Test POST request of User resource with invalid color value.
        Error should be returned.
        """
        self.valid_data["color"] = "abcdef"
        exp_res = {"user": {"color": "Passed value is not hexadecimal format."}}

        self.make_test_request(exp_res)

    def test_post_background_invalid_extension(self):
        """
        Test POST request of User resource with invalid background image file
        (unallowed extension).
        Error should be returned.
        """
        self.valid_data["background"] = (BytesIO(b'test'), 'back.mp4')
        exp_res = {"user": {"background": "Invalid extension"}}

        self.make_test_request(exp_res)

    def test_post_avatar_invalid_extension(self):
        """
        Test POST request of User resource with invalid avatar image file
        (unallowed extension).
        Error should be returned.
        """
        self.valid_data["avatar"] = (BytesIO(b'test'), 'myphoto.xcf')
        exp_res = {"user": {"avatar": "Invalid extension"}}

        self.make_test_request(exp_res)

    def test_post_background_the_same_name(self):
        """
        Test POST request of User resource with background file that has the
        name that is already used.
        Name of newer file should be modified and serialized user should be
        returned.
        """
        name = self.valid_data["background"][1]

        with open(os.path.join(self.app.config["UPLOAD"], name), 'w') as f:
            f.write('this is test')

        res = self.client.post("/api/user", data=self.valid_data,
                                        content_type="multipart/form-data")

        files = filter(lambda x: x.startswith(name.split(".")[0]),
                                os.listdir(self.app.config["UPLOAD"]))

        self.assertEqual(len(list(files)), 2)

    def make_test_request(self, exp_res):
        """
        Makes POST request to User resource and checks if expected response has
        been returned.
        """
        res = self.client.post("/api/user", data=self.valid_data,
                                        content_type="multipart/form-data")
        self.assertEqual(exp_res, self.json(res))


class UserItemResourceTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.city = City(name="New York", region="New York", country="USA")

        self.user_data = {
            "email": "martha@amiga.gmail",
            "username": "espanolmuybien",
            "first_name": "Martha",
            "last_name": "Dos Santos",
            "city": "ChIJz-fn0PHGPUcRcrIsc6p0wI0",
            "birthdate": "1989-06-04",
            "password": "superSecretPassword123",
            "background": (BytesIO(b"data"), "back.jpg"),
            "avatar": (BytesIO(b"data"), "avatar.png"),
            "color": "#abde"
        }

        self.client.post("/api/user", data=self.user_data,
                                        content_type="multipart/form-data")

        self.user = User.query.first()

        [db.session.add(x) for x in [self.user, self.city]]
        db.session.commit()

        data = {
            'username': self.user.username,
            'password': self.user_data["password"]
        }

        self.client.post('/login', data=data)

    def tearDown(self):
        super().tearDown()

        for filename in ["avatar.png", "back.jpg", "new_back.jpg"]:
            path = os.path.join(BASE_DIR, "static/upload/" + filename)

            if os.path.isfile(path):
                os.remove(path)

    def test_get(self):
        """
        Test GET request of User item resource with valid id of user.
        Serialized user should be returned.
        """
        exp_res = {
            "color": "#abde",
            "city": {
                "longitude": 21.1596321,
                "country": "Polska",
                "id": 1,
                "latitude": 49.6546159,
                "place_id": "ChIJz-fn0PHGPUcRcrIsc6p0wI0",
                "name": "Gorlice",
                "region": "ma\u0142opolskie"
            },
            "id": 1,
            "avatar": "http://localhost/static/upload/avatar.png",
            "friends": [],
            "last_name": "Dos Santos",
            "username": "espanolmuybien",
            "first_name": "Martha",
            "background": "http://localhost/static/upload/back.jpg",
            "full_name": "Martha Dos Santos",
            "birthdate": "1989-06-04",
            "email": "martha@amiga.gmail",
            "views": 0
        }

        res = self.client.get("/api/user/" + str(self.user.id))

        self.assertEqual(exp_res, self.json(res))

    def test_put_file_new(self):
        """
        Test PUT request of User item resource with valid id of user and image
        file in data as background.
        Serialized user with new background should be returned and old
        background image file should be deleted.
        """
        data = {"background": (BytesIO(b"data"), "new_back.jpg")}

        path = os.path.join(BASE_DIR, "static/upload/" + self.user.background)
        self.assertTrue(os.path.isfile(path))

        res = self.client.put("/api/user/" + str(self.user.id), data=data,
                                            content_type="multipart/form-data")

        self.assertFalse(os.path.isfile(path))
        self.assertEqual("http://localhost/static/upload/" + data["background"][1],
                        self.json(res)["background"])

    def test_put_file_none(self):
        """
        Test PUT request of User item resource with valid id of user and empty
        string in data as avatar.
        Serialized user with avatar as null should be returned and old avatar
        image file should be deleted.
        """
        data = {"avatar": ''}

        path = os.path.join(BASE_DIR, "static/upload/" + self.user.avatar)
        self.assertTrue(os.path.isfile(path))

        res = self.client.put("/api/user/" + str(self.user.id), data=data)

        self.assertFalse(os.path.isfile(path))
        self.assertEqual(self.json(res)["avatar"], None)

    def test_put_normal_value(self):
        """
        Test PUT request of User item resource with valid id of user and normal
        data (integer or string).
        Serialized user with changed data should be returned.
        """
        data = {"username": "martha_from_spain123"}

        res = self.client.put("/api/user/" + str(self.user.id), data=data)
        self.assertEqual(self.json(res)["username"], data["username"])

    def test_put_other_profile(self):
        """
        Test PUT request of User item resource with id of user that is not the
        same as current user's id.
        Error should be returned.
        """
        new_user = User(username="jackie", first_name="Jack", last_name="Ruby")
        db.session.add(new_user)
        db.session.commit()

        data = {"last_name": "Mertens"}

        res = self.client.put("/api/user/" + str(new_user.id), data=data)
        self.assertEqual(self.json(res),
                        {"status": "You can modify only your account."})

    def test_put_not_found(self):
        """
        Test PUT request of User item resource with id of user that doesn't
        exist.
        Error should be returned.
        """
        res = self.client.put("/api/user/123", data={})
        self.assertEqual(self.json(res),
                            {"status": "User object doesn't exist."})

    def test_put_change_password_valid(self):
        """
        Test PUT request of User item with valid password.
        Password should be changed.
        """
        data = {
            "old_password": self.user_data["password"],
            "password": "newSuperDuperPassword123"
        }

        self.client.put("/api/currentuser", data=data)

        user = User.query.first()
        self.assertTrue(user.verify_password(data["password"]))

    def test_put_change_password_none_old(self):
        """
        Test PUT request of User item without old password.
        Error should be returned.
        """
        data = {"password": "newSuperDuperPassword123"}

        exp_res = {'user': {'old_password': 'Old password is required.'}}
        res = self.client.put("/api/currentuser", data=data)

        self.assertEqual(self.json(res), exp_res)

    def test_put_change_password_invalid_old(self):
        """
        Test PUT request of User item with invalid old password.
        Error should be returned.
        """
        data = {
            "old_password": "thisIsInvalidOldPassword",
            "password": "newSuperDuperPassword123"
        }

        exp_res = {'user': {'old_password': 'Old password is invalid.'}}
        res = self.client.put("/api/currentuser", data=data)

        self.assertEqual(self.json(res), exp_res)

    def test_put_change_password_invalid_new(self):
        """
        Test PUT request of User item with invalid new password.
        Error should be returned.
        """
        data = {
            "old_password": self.user_data["password"],
            "password": "thispasswordisnotcorrect"
        }

        exp_res = {"user": {"password": "Password needs to have at least one small and big letter and one digit."}}
        res = self.client.put("/api/currentuser", data=data)

        self.assertEqual(self.json(res), exp_res)

    def test_put_change_password_invalid_len_new(self):
        """
        Test PUT request of User item with new password with invalid length.
        Error should be returned.
        """
        data = {
            "old_password": self.user_data["password"],
            "password": "tshrt"
        }

        exp_res = {"user": {"password": "Password needs to have minimum 6 and maximum 32 chars."}}
        res = self.client.put("/api/currentuser", data=data)

        self.assertEqual(self.json(res), exp_res)


class FriendsResourceTest(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

        city = City(name="Los Angeles", region="California", country="USA")
        self.friend = User(username="francie", first_name="Francesca",
                                                last_name="Totti", city=city)

        db.session.add_all([city, self.friend])
        db.session.commit()

        self.user.add_friend(self.friend)

    def test_get(self):
        """
        Test GET request of Friends resource.
        Serialized friends should be returned.
        """
        exp_res = [
            {
            "first_name": "Francesca",
            "email": None,
            "background": None,
            "birthdate": None,
            "username": "francie",
            "color": None,
            "city": {
                "latitude": None,
                "country": "USA",
                "region": "California",
                "id": 1,
                "longitude": None,
                "name": "Los Angeles",
                "place_id": None
            },
            "id": 2,
            "avatar": None,
            "full_name": None,
            "last_name": "Totti",
            "views": 0}
        ]

        res = self.client.get("/api/friends")
        self.assertEqual(self.json(res), exp_res)

    def test_delete_friend_valid(self):
        """
        Test DELETE request of Friends resource with valid friend id.
        User should have been removed from current user friends.
        """
        msg = "Friend {} has been removed.".format(self.friend.username)
        res = self.client.delete("/api/friends",
                                            data={"friend_id": self.friend.id})
        self.assertEqual(self.json(res), {"status": msg})
        self.assertEqual(self.user.friends.count(), 0)

    def test_delete_friend_invalid(self):
        """
        Test DELETE request of Friends resource with invalid friend id.
        User should have been removed from current user friends.
        """
        exp_res = { "status": "Friend with that id doesn't exist."}
        res = self.client.delete("/api/friends", data={"friend_id": 123456})
        self.assertEqual(self.json(res), exp_res)
        self.assertNotEqual(self.user.friends.count(), 0)


class InvitationResourceTest(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

    def test_get(self):
        """
        Test GET request of Invitation resource.
        Serialized received and posted invitations should be returned.
        """
        user_2, user_3 = self.handle_invitation_users()

        i1 = Invitation(sender=user_2, invites=self.user)
        i2 = Invitation(sender=self.user, invites=user_3)
        [db.session.add(x) for x in [i1, i2]]
        db.session.commit()

        exp_res = {
            'posted': [
                {
                'id': 2,
                'invites': {
                    'views': 0,
                    'birthdate': None,
                    'city': None,
                    'last_name': 'Piskor',
                    'id': 3,
                    'first_name': 'Marcin',
                    'background': None,
                    'color': None,
                    'email': None,
                    'full_name': None,
                    'username': 'm_pis',
                    'avatar': None
                    }
                }
            ],
            'received': [
                {
                'id': 1,
                'sender': {
                    'views': 0,
                    'birthdate': None,
                    'city': None,
                    'last_name': 'Lopez',
                    'id': 2,
                    'first_name': 'Maria',
                    'background': None,
                    'color': None,
                    'email': None,
                    'full_name': None,
                    'username': 'mari_b',
                    'avatar': None
                    }
                }
            ]
        }

        res = self.client.get('/api/invitation')
        self.assertEqual(self.json(res), exp_res)

    def test_post_valid(self):
        """
        Test POST request of Invitation resource with proper id of user in data.
        Serialized invitation should be returned.
        """
        user_2 = self.handle_invitation_users()[0]
        exp_res = {
            'invites': {
                'first_name': 'Maria',
                'avatar': None,
                'city': None,
                'color': None,
                'username': 'mari_b',
                'email': None,
                'background': None,
                'birthdate': None,
                'full_name': None,
                'id': 2,
                'views': 0,
                'last_name': 'Lopez'
            },
            'id': 1
        }

        res = self.client.post("/api/invitation", data={'user_id': user_2.id})
        self.assertEqual(self.json(res), exp_res)

    def test_post_invite_yourself(self):
        """
        Test POST request of Invitation resource with id of user in data that
        is the same as id of current user.
        Error should be returned.
        """
        exp_res = {"user_id": "You cannot invite yourself."}
        res = self.client.post("/api/invitation", data={'user_id': self.user.id})

        self.assertEqual(self.json(res), exp_res)

    def test_post_invited(self):
        """
        Test POST request of Invitation resource with id of user in data that
        has been already invited by current user.
        Error should be returned.
        """
        user_2 = self.handle_invitation_users()[0]
        exp_res = {'user_id': "You've already invited user mari_b."}

        data = {'user_id': user_2.id}
        req = lambda: self.client.post("/api/invitation", data=data)

        req()
        self.assertEqual(self.json(req()), exp_res)

    def test_post_friend(self):
        """
        Test POST request of Invitation resource with id of user in data that
        is already a friend of current user.
        Error should be returned.
        """
        user_2 = self.handle_invitation_users()[0]
        user_2.add_friend(self.user)

        exp_res = {'user_id': "User mari_b is already your friend."}

        res = self.client.post("/api/invitation", data={'user_id': user_2.id})
        self.assertEqual(self.json(res), exp_res)

    def test_post_user_invited(self):
        """
        Test POST request of Invitation resource with id of user in data that
        has already invited current user.
        Error should be returned.
        """
        user_2 = self.handle_invitation_users()[0]
        inv = Invitation(sender=user_2, invites=self.user)

        db.session.add(inv)
        db.session.commit()

        exp_res = {'user_id': "User mari_b has already invited you."}

        res = self.client.post("/api/invitation", data={'user_id': user_2.id})
        self.assertEqual(self.json(res), exp_res)

    def test_post_user_doesnt_exist(self):
        """
        Test POST request of Invitation resource with id of user that doesn't
        exist.
        Error should be returned.
        """
        exp_res = {"user_id": "User doesn't exist."}

        res = self.client.post("/api/invitation", data={'user_id': 12345})
        self.assertEqual(self.json(res), exp_res)


class InvitationItemResourceTest(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

    def test_get_invites(self):
        """
        Test GET request of Invitation item resource with id of invitation that
        is related with current user as invites.
        Serialized invitation should be returned.
        """
        user_2 = self.handle_invitation_users()[0]

        inv = Invitation(sender=user_2, invites=self.user)
        db.session.add(inv)
        db.session.commit()

        exp_res = {
            'sender': {
                'avatar': None,
                'id': 2,
                'views': 0,
                'email': None,
                'city': None,
                'first_name': 'Maria',
                'background': None,
                'last_name': 'Lopez',
                'color': None,
                'birthdate': None,
                'full_name': None,
                'username': 'mari_b'
                },
            'id': 1
        }

        res = self.client.get("/api/invitation/" + str(inv.id))

        self.assertEqual(self.json(res), exp_res)

    def test_get_sender(self):
        """
        Test GET request of Invitation resource with id of invitation item that
        is related with current user as sender.
        Serialized invitation should be returned.
        """
        user_2 = self.handle_invitation_users()[0]

        inv = Invitation(sender=self.user, invites=user_2)
        db.session.add(inv)
        db.session.commit()

        exp_res = {
            'invites': {
                'avatar': None,
                'id': 2,
                'views': 0,
                'email': None,
                'city': None,
                'first_name': 'Maria',
                'background': None,
                'last_name': 'Lopez',
                'color': None,
                'birthdate': None,
                'full_name': None,
                'username': 'mari_b'
                },
            'id': 1
        }

        res = self.client.get("/api/invitation/" + str(inv.id))

        self.assertEqual(self.json(res), exp_res)

    def test_get_invitation_no_related(self):
        """
        Test GET request of Invitation resource with id of invitation item that
        is not related with current user.
        Error should be returned.
        """
        user_2, user_3 = self.handle_invitation_users()

        inv = Invitation(sender=user_3, invites=user_2)
        db.session.add(inv)
        db.session.commit()

        exp_res = {'status': "Invitation doesn't exist."}

        res = self.client.get("/api/invitation/" + str(inv.id))
        self.assertEqual(self.json(res), exp_res)

    def test_get_doesnt_exist(self):
        """
        Test GET request of Invitation resource with id of invitation that
        does not exist.
        Error should be returned.
        """
        exp_res = {'status': "Invitation doesn't exist."}

        res = self.client.get("/api/invitation/12345")
        self.assertEqual(self.json(res), exp_res)

    def test_delete_invites(self):
        """
        Test DELETE request of Invitation resource with id of invitation that
        is related with current user as invites.
        Invitation object should be deleted and info about that should be
        returned.
        """
        user_2 = self.handle_invitation_users()[0]

        inv = Invitation(sender=user_2, invites=self.user)
        db.session.add(inv)
        db.session.commit()

        exp_res = {"status": "Invitation has been deleted."}

        res = self.client.delete("/api/invitation/" + str(inv.id))
        self.assertEqual(self.json(res), exp_res)
        self.assertEqual(Invitation.query.count(), 0)

    def test_delete_sender(self):
        """
        Test DELETE request of Invitation resource with id of invitation that
        is related with current user as sender.
        Invitation object should be deleted and info about that should be
        returned.
        """
        user_2 = self.handle_invitation_users()[0]

        inv = Invitation(sender=self.user, invites=user_2)
        db.session.add(inv)
        db.session.commit()

        exp_res = {"status": "Invitation has been deleted."}

        res = self.client.delete("/api/invitation/" + str(inv.id))
        self.assertEqual(self.json(res), exp_res)
        self.assertEqual(Invitation.query.count(), 0)

    def test_delete_no_related(self):
        """
        Test DELETE request of Invitation resource with id of invitation that
        is not related with current user.
        Error should be returned.
        """
        user_2, user_3 = self.handle_invitation_users()

        inv = Invitation(sender=user_2, invites=user_3)
        db.session.add(inv)
        db.session.commit()

        exp_res = {"status": "Invitation doesn't exist."}

        res = self.client.delete("/api/invitation/" + str(inv.id))
        self.assertEqual(self.json(res), exp_res)
        self.assertEqual(Invitation.query.count(), 1)

    def test_delete_doesnt_exist(self):
        """
        Test DELETE request of Invitation resource with id of invitation that
        does not exist.
        Error should be returned.
        """
        exp_res = {'status': "Invitation doesn't exist."}

        res = self.client.delete("/api/invitation/12345")
        self.assertEqual(self.json(res), exp_res)


class AcceptInvitationResource(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

    def test_get_invites(self):
        """
        Test GET request of Accept invitation resource with id of invitation
        that is related with current user as invites.
        Serialized invitation should be returned.
        """
        user_2 = self.handle_invitation_users()[0]

        inv = Invitation(sender=user_2, invites=self.user)
        db.session.add(inv)
        db.session.commit()

        exp_res = {'status': "You've accepted user mari_b invitation."}

        res = self.client.get("/api/accept_invitation/" + str(inv.id))

        self.assertEqual(self.json(res), exp_res)
        self.assertTrue(user_2 in self.user.friends.all())
        self.assertEqual(Invitation.query.count(), 0)

    def test_get_sender(self):
        """
        Test GET request of Accept invitation resource with id of invitation
        that is related with current user as sender.
        Error should be returned.
        """
        user_2 = self.handle_invitation_users()[0]

        inv = Invitation(sender=self.user, invites=user_2)
        db.session.add(inv)
        db.session.commit()

        exp_res = {'status': "Invitation doesn't exist."}

        res = self.client.get("/api/accept_invitation/" + str(inv.id))
        self.assertEqual(self.json(res), exp_res)
        self.assertEqual(Invitation.query.count(), 1)

    def test_get_no_related(self):
        """
        Test GET request of Accept invitation resource with id of invitation
        that is not related with current user.
        Error should be returned.
        """
        user_2, user_3 = self.handle_invitation_users()

        inv = Invitation(sender=user_3, invites=user_2)
        db.session.add(inv)
        db.session.commit()

        exp_res = {'status': "Invitation doesn't exist."}

        res = self.client.get("/api/accept_invitation/" + str(inv.id))
        self.assertEqual(self.json(res), exp_res)
        self.assertEqual(Invitation.query.count(), 1)

    def test_get_doesnt_exist(self):
        """
        Test GET request of Accept invitation resource with id of invitation
        that doesn't exist.
        Error should be returned.
        """
        exp_res = {'status': "Invitation doesn't exist."}

        res = self.client.get("/api/accept_invitation/1234")
        self.assertEqual(self.json(res), exp_res)


class CockpitSearchResourceTest(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

        self.user.first_name = "Junior"
        self.user.last_name =  "Da Silva"
        self.user.generate_full_name()

        city_1 = City(name="Tel Awiw", country="Izrael")
        city_2 = City(name="Waszyngton", country="USA")
        [db.session.add(x) for x in [self.user, city_1, city_2]]
        db.session.commit()

        self.serialized_user = {
            'id': 1,
            'birthdate': None,
            'background': None,
            'views': 0,
            'first_name': 'Junior',
            'avatar': None,
            'username': 'tester',
            'color': None,
            'city': None,
            'last_name': 'Da Silva',
            'full_name': 'Junior Da Silva',
            'email': 'tester@tester.tester'
        }

    def test_get_normal(self):
        """
        Test GET request of Cockpit search resource with string as query
        parameter.
        Serialized user and city which names start with the query should be
        returned.
        """
        exp_res = {
            'cities': [
                {
                'id': 1,
                'latitude': None,
                'country': 'Izrael',
                'name': 'Tel Awiw',
                'longitude': None,
                'region': None,
                'place_id': None
                }
            ],
            'users': [self.serialized_user]
        }

        res = self.client.get("/api/cockpit_search?query=te")
        self.assertEqual(self.json(res), exp_res)

    def test_get_by_email(self):
        """
        Test GET request of Cockpit search resource with e-mail of user as query
        parameter.
        Serialized user with e-mail that corresponds to the query should be
        returned.
        """
        exp_res = {'cities': [], 'users': [self.serialized_user]}

        res = self.client.get("/api/cockpit_search?query=tester@tester.tester")
        self.assertEqual(self.json(res), exp_res)

    def test_get_by_full_name(self):
        """
        Test GET request of Cockpit search resource with full name of user as
        query parameter.
        Serialized user with full name that corresponds to the query should be
        returned.
        """
        exp_res = {'cities': [], 'users': [self.serialized_user]}

        res = self.client.get("/api/cockpit_search?query=Junior+Da+Silva")
        self.assertEqual(self.json(res), exp_res)


class CityItemTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

    def test_get_normal(self):
        """
        Test GET request of City item resource with valid id of city.
        Serialized version of City should be returned.
        """
        user = User.query.first()

        city = City(name="Los Angeles", region="Kalifornia", country="USA")
        db.session.add(city)
        db.session.commit()

        friend = User(username="Maniek", email="mani@ek.pl", city=city)
        db.session.add(friend)
        db.session.commit()

        friend.add_friend(user)

        moment = Moment(content="photo.jpg", author=friend, city=city)
        moment.viewers.append(user)

        db.session.add(moment)
        db.session.commit()

        exp_res = {
            'name': 'Los Angeles',
            'country': 'USA',
            'users': [
                {
                    'background': None,
                    'first_name': None,
                    'email': 'mani@ek.pl',
                    'full_name': None,
                    'avatar': None,
                    'id': 2,
                    'color': None,
                    'birthdate': None,
                    'last_name': None,
                    'views': 0,
                    'username': 'Maniek'
                }
            ],
            'region': 'Kalifornia',
            'id': 1,
            'moments': [
                {
                    'background': None,
                    'first_name': None,
                    'email': 'mani@ek.pl',
                    'full_name': None,
                    'avatar': None,
                    'id': 2,
                    'color': None,
                    'birthdate': None,
                    'last_name': None,
                    'views': 0,
                    'username': 'Maniek'
                }
            ],
            'latitude': None,
            'longitude': None,
            'place_id': None
        }


        res = self.client.get("/api/city/" + str(city.id))
        self.assertEqual(self.json(res), exp_res)

    def test_get_none(self):
        """
        Test GET request of City item resource with invalid id of city.
        Error should be returned.
        """
        exp_res = {"city": {"id": "City with that id doesn't exist."}}

        res = self.client.get("/api/city/123")
        self.assertEqual(self.json(res), exp_res)

if __name__ == "__main__":
    unittest.main()
