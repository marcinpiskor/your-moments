import unittest
from app import create_app, config

class AppTest(unittest.TestCase):
    def test_create_app_function(self):
        for con in config:
            new_app = create_app(con)
            config_type = config[con]

            for attr in dir(config_type):
                if not attr.startswith("__"):
                    self.assertEqual(new_app.config[attr], getattr(config_type, attr))

if __name__ == "__main__":
    unittest.main()
