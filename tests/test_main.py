import os
import json
import base64
import unittest
from test_auth import TestMixin
from io import BytesIO
from app import db
from app.auth.models import User, City
from app.main.models import Moment

class ActiveFriendTest(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

        user2, user3 = self.handle_invitation_users()

        # Creating Moment objects related with current user (as viewer) and
        # current user friend (as author).
        for u in [user2, user3]:
            self.user.add_friend(u)
            self.create_moment(content=u.username+".jpg", duration=10, author=u)

    def test_normal_get(self):
        """
        Tests normal GET request (without query parameters) of Active friends
        resource.
        Serialized users should be returned.
        """
        exp_res = {
            'friends': [
                {
                    'avatar': None,
                    'views': 0,
                    'last_name': 'Piskor',
                    'id': 3,
                    'first_name': 'Marcin',
                    'city': None,
                    'username': 'm_pis',
                    'color': None,
                    'background': None,
                    'birthdate': None,
                    'full_name': None,
                    'email': None
                },
                {
                    'avatar': None,
                    'views': 0,
                    'last_name': 'Lopez',
                    'id': 2,
                    'first_name': 'Maria',
                    'city': None,
                    'username': 'mari_b',
                    'color': None,
                    'background': None,
                    'birthdate': None,
                    'full_name': None,
                    'email': None
                }
            ],
            'paginate': False
        }

        res = self.client.get("/api/activefriends")

        content = self.json(res)
        [f.pop("last_active") for f in content["friends"]]

        self.assertEqual(content, exp_res)

    def test_last_active(self):
        """
        Test last active property of User model.
        New last active property of one of the current user friends should be
        updated.
        """
        exp_res = {}

        # Creating dict with Moment author username as key and Moment date as
        # value.
        for m in self.user.friends_moments.all():
            exp_res[m.author.username] = m.date.isoformat() + "Z"

        self.last_active_req(exp_res)

        # Getting current user friend and creating new Moment object by it.
        u = self.user.friends.first()
        m = self.create_moment(content=u.username+".jpg", duration=10, author=u)

        exp_res[u.username] = m.date.isoformat() + "Z"

        self.last_active_req(exp_res)

    def test_query_get(self):
        """
        Tests GET request with query parameters of Active friends resource.
        Serialized users should be returned.
        """
        u = User(username="presidente", first_name="Donald", last_name="Obama",
                                                                    views=21)
        db.session.add(u)
        db.session.commit()
        self.user.add_friend(u)

        self.create_moment(content="whitehouse.mp4", author=u, duration=20)

        exp_res = {
            'hottest': {
                'first_name': 'Donald',
                'views': 21,
                'city': None,
                'id': 4,
                'color': None,
                'full_name': None,
                'email': None,
                'avatar': None,
                'birthdate': None,
                'background': None,
                'username': 'presidente',
                'last_name': 'Obama'
            },
            'paginate': True,
            'has_next': True
        }

        exp_res["friends"] = [
            {
                'first_name': 'Marcin',
                'views': 0,
                'city': None,
                'id': 3,
                'color': None,
                'full_name': None,
                'email': None,
                'avatar': None,
                'birthdate': None,
                'background': None,
                'username': 'm_pis',
                'last_name': 'Piskor'
            }
        ]

        self.query_req(exp_res, 1)

        exp_res["friends"] = [
            {
                'first_name': 'Maria',
                'full_name': None,
                'city': None,
                'username': 'mari_b',
                'birthdate': None,
                'email': None,
                'background': None,
                'last_name': 'Lopez',
                'id': 2,
                'color': None,
                'avatar': None,
                'views': 0
            }
        ]
        exp_res["has_next"] = False

        self.query_req(exp_res, 2)

    def query_req(self, exp_res, page):
        """
        Makes active friends resource query GET and checks if returns expected
        responce.

        Argument/s:
            exp_res - expected response of active friends GET request
            page - number of actual page (query parameter)
        """
        res = self.client.get("/api/activefriends?page_number={}&per_page=1".format(page))
        content = self.json(res)

        for friend in [content["friends"][0], content["hottest"]]:
            friend.pop("last_active")

        self.assertEqual(exp_res, content)

    def last_active_req(self, exp_res):
        """
        Checks if last active property of user has proper value.

        Argument/s:
            exp_res - expected response of active friends GET request
        """
        res = self.client.get("/api/activefriends")

        content = self.json(res)
        content = {f["username"]: f["last_active"] for f in content["friends"]}

        self.assertEqual(content, exp_res)

    def create_moment(self, **kwargs):
        """
        Creates Moment object and sets current user as viewer of that moment.
        """
        m = Moment(**kwargs)
        m.viewers.append(self.user)

        db.session.add(m)
        db.session.commit()

        return m


class MomentResourceTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()

        user2, user3 = self.handle_invitation_users()

        for u in [user2, user3]:
            self.user.add_friend(u)

        self.valid_data = data = {
            "viewers": json.dumps([f.id for f in self.user.friends.all()]),
            "content": (BytesIO(b'data'), "photo.png"),
            "duration": 20
        }

        self.success_res = {'status': 'Moment has been created.'}

    def test_post_valid_img_file(self):
        """
        Test POST request of moment resource with valid image data.
        Moment object should be created.
        """
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")

        self.assertEqual(self.json(res), self.success_res)
        self.assertEqual(Moment.query.count(), 1)
        self.assertEqual(Moment.query.first().viewers.count(),
                                        self.user.friends.count())

    def test_post_viewers_invalid_value(self):
        """
        Test POST request of moment resource with invalid viewers.
        Errors should be returned.
        """
        self.valid_data["viewers"] = 1

        exp_res = {'moment': {'viewers': 'Viewers should be a stringified list/array with id numbers of friends.'}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_viewers_none(self):
        """
        Test POST request of moment resource without viewers.
        Errors should be returned.
        """
        del self.valid_data["viewers"]

        exp_res = {'moment': {'viewers': "Field is required."}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_image_duration_invalid(self):
        """
        Test POST request of moment resource with invalid image duration.
        Errors should be returned.
        """
        self.valid_data["duration"] = 0

        exp_res = {'moment': {'duration': 'Duration of image moment has to be equal or greater than 1.'}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_image_duration_none(self):
        """
        Test POST request of moment resource without image duration.
        Errors should be returned.
        """
        del self.valid_data["duration"]

        exp_res = {'moment': {'duration': 'Duration is required for moments that contains images.'}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_valid_city(self):
        """
        Test POST request of moment resource with valid data (city).
        Moment and City objects should be created.
        """
        self.valid_data['city'] = "ChIJz-fn0PHGPUcRcrIsc6p0wI0"

        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), self.success_res)
        self.assertEqual(City.query.count(), 1)
        self.assertEqual(Moment.query.first().city, City.query.first())

    def test_post_none_city(self):
        """
        Test POST request of moment resource with invalid city value (blank).
        Errors should be returned.
        """
        self.valid_data['city'] = ""

        exp_res = {"moment": {"city": "Choose right localization."}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_none_city(self):
        """
        Test POST request of moment resource with invalid city value (wrong id).
        Errors should be returned.
        """
        self.valid_data['city'] = "invalid_place_id"

        exp_res = {"moment": {"city": "City doesn't exist."}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_image_invalid_extension(self):
        """
        Test POST request of moment resource with invalid image extension.
        Errors should be returned.
        """
        self.valid_data['content'] = (BytesIO(b'data'), "photo.gif")

        exp_res = {"moment": {"content": "Image has invalid extension (only jpg, jpeg and PNG are allowed )"}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_none_content(self):
        """
        Test POST request of moment resource without content.
        Errors should be returned.
        """
        del self.valid_data['content']

        exp_res = {"moment": {"content": "Field is required."}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_valid_img_base64(self):
        """
        Test POST request of moment resource with valid data (base64 image).
        Moment object should be created.
        """
        file_path = os.path.join(os.getcwd(), 'app/static/images/1.jpg')

        with open(file_path, 'rb') as f:
            self.valid_data["content"] = base64.encodebytes(f.read())

        res = self.client.post("/api/moment", data=self.valid_data)

        self.assertEqual(self.json(res), self.success_res)
        self.assertEqual(Moment.query.count(), 1)

    def test_post_valid_video(self):
        """
        Test POST request of moment resource with valid video data.
        Moment object should be created.
        """
        self.video_to_bytes('valid')

        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")

        self.assertEqual(self.json(res), self.success_res)
        self.assertEqual(Moment.query.count(), 1)
        self.assertEqual(8.703333, Moment.query.first().duration)

    def test_post_invalid_video_duration(self):
        """
        Test POST request of moment resource with invalid video duration.
        Errors should be returned.
        """
        self.video_to_bytes('invalid')

        exp_res = {'moment': {'duration': 'Duration of video moment has to be greater 0 and lesser than 11 seconds.'}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_broken_file(self):
        """
        Test POST request of moment resource with invalid vide file (broken).
        Errors should be returned.
        """
        self.valid_data["content"] = (BytesIO(b'data'), "video.mp4")

        exp_res = {'moment': {'content': 'Video file is broken.'}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def test_post_video_invalid_extension(self):
        """
        Test POST request of moment resource with invalid video file extension.
        Errors should be returned.
        """
        self.valid_data["content"] = (BytesIO(b'data'), "video.avi")

        exp_res = {'moment': {'content': 'Video has invalid extension (only MP4, WebM and 3gp allowed )'}}
        res = self.client.post("/api/moment", data=self.valid_data,
                                    content_type="multipart/form-data")
        self.assertEqual(self.json(res), exp_res)

    def video_to_bytes(self, name):
        """
        Converts video to bytes.

        Argument/s:
            name - name of video file in 'images' directory
        """
        file_path = os.path.join(os.getcwd(),
                        'app/static/images/vid_{}.mp4'.format(name))

        with open(file_path, 'rb') as f:
            self.valid_data["content"] = (BytesIO(f.read()), 'video.mp4')


class UserMomentsResourceTestCase(TestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.login_user()
        user2, user3 = self.handle_invitation_users()

        for u in [user2, user3]:
            self.user.add_friend(u)

        self.friends_serialized = {
            'id': 2,
            'views': 0,
            'email': None,
            'background': None,
            'color': None,
            'first_name': 'Maria',
            'last_name': 'Lopez',
            'city': None,
            'username': 'mari_b',
            'full_name': None,
            'birthdate': None,
            'avatar': None
        }

    def test_valid_get(self):
        """
        Test GET request of user moments resource with id of user that is friend
        of current user and  has created Moment objects with current user as
        viewer.
        Serialized moments should be displayed, one by one.
        """
        friend = self.user.friends.first()

        m1 = Moment(author=friend, duration=20, content="photo.png")
        m2 = Moment(author=friend, duration=5, content="movie.mp4")
        [m.viewers.append(self.user) for m in [m1, m2]]

        db.session.add_all([m1, m2])
        db.session.commit()

        exp_res = {
            'moment': {
                'content': 'http://localhost/static/upload/photo.png',
                'id': 1,
                'city': None,
                'duration': 20,
            },
            'author': self.friends_serialized,
            'has_next': True
        }

        res = self.prepare_response(friend.id)
        self.assertEqual(res, exp_res)

        exp_res["author"]["views"] += 1
        exp_res['has_next'] = False
        exp_res['moment'] = {
            'duration': 5,
            'content': 'http://localhost/static/upload/movie.mp4',
            'id': 2,
            'city': None
        }

        res = self.prepare_response(friend.id)
        self.assertEqual(res, exp_res)

    def test_get_without_moments(self):
        """
        Test GET request of user moments resource with id of user that is friend
        of current user but hasn't created Moment objects with current user as
        viewer.
        Serialized user should be displayed only.
        """
        exp_res = {
            "author": self.friends_serialized, "moment": None, "has_next": False
        }

        friend = self.user.friends.first()
        res = self.client.get('/api/user_moments/' + str(friend.id))

        self.assertEqual(self.json(res), exp_res)

    def test_get_no_friend(self):
        """
        Test GET request of user moments resource with id of user that isn't
        friend of current user.
        Nothing about user or moment should be displayed.
        """
        exp_res =  {'moment': None, 'has_next': False, 'author': {}}
        res = self.client.get('/api/user_moments/123')
        self.assertEqual(self.json(res), exp_res)

    def prepare_response(self, friend_id):
        """
        Makes GET request of user moments and removes date from serialized
        moment.

        Argument/s:
            friend_id - id of friend
        """
        res = self.client.get('/api/user_moments/' + str(friend_id))
        content = self.json(res)
        del content["moment"]["date"]

        return content

if __name__ == "__main__":
    unittest.main()
