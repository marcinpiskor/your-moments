from app import db, create_app
from app.auth.models import SuperUserCommand
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

app = create_app("default")
manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)
manager.add_command('createsuperuser', SuperUserCommand)

if __name__ == "__main__":
    manager.run()
